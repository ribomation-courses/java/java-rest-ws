Instructions
====

Ensure you have NodeJS / NPM installed.
* [Node Downloads](https://nodejs.org/en/download/)

Install the dependencies

    npm install

Run the Swagger UI

    node swagger.js

