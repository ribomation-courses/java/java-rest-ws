package ribomation.jdk11_http_client.json;

import java.util.Objects;

public class Person {
    private static int    nextId = 1;
    private        int    id     = -1;
    private        String name;
    private        int    age;

    public Person() { }
    public Person(String name, int age) {
        this(nextId++, name, age);
    }
    public Person(int id, String name, int age) {
        this.id = id;
        this.name = name;
        this.age = age;
    }

    @Override
    public String toString() {
        return String.format("Person{id:%d, name:%s, age:%d}", id, name, age);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Person person = (Person) o;
        return id == person.id &&
                age == person.age &&
                Objects.equals(name, person.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, age);
    }

    public int getId() {
        return id;
    }
    public String getName() {
        return name;
    }
    public int getAge() {
        return age;
    }
}

