package ribomation.jdk11_http_client.json;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.io.IOException;
import java.lang.reflect.Type;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.util.ArrayList;
import java.util.List;


public class JsonCRUD<T> {
    private final String   baseUri;
    private final String   resource;
    private final Class<T> type;

    public JsonCRUD(String baseUri, String resource, Class<T> type) {
        this.baseUri = baseUri;
        this.resource = resource;
        this.type = type;
    }

    private URI mkResource() {
        return URI.create(String.format("%s/%ss", baseUri, resource));
    }

    private URI mkResource(int id) {
        return URI.create(String.format("%s/%s/%d", baseUri, resource, id));
    }

    public T read(int id) throws IOException, InterruptedException {
        var req = HttpRequest.newBuilder()
                .uri(mkResource(id))
                .GET()
                .build();
        var client = HttpClient.newHttpClient();
        var res    = client.send(req, new JsonBodyHandler<>(type));
        return res.body();
    }

    public T update(int id, T obj) throws IOException, InterruptedException {
        var req = HttpRequest.newBuilder()
                .uri(mkResource(id))
                .PUT(HttpRequest.BodyPublishers.ofString(new Gson().toJson(obj)))
                .build();
        var client = HttpClient.newHttpClient();
        var res    = client.send(req, new JsonBodyHandler<>(type));
        return res.body();
    }

    public int delete(int id) throws IOException, InterruptedException {
        var req = HttpRequest.newBuilder()
                .uri(mkResource(id))
                .DELETE()
                .build();
        var client = HttpClient.newHttpClient();
        var res    = client.send(req, HttpResponse.BodyHandlers.discarding());
        return res.statusCode();
    }

    public  T create(T obj) throws IOException, InterruptedException {
        var req = HttpRequest.newBuilder()
                .uri(mkResource())
                .POST(HttpRequest.BodyPublishers.ofString(new Gson().toJson(obj)))
                .build();
        var client = HttpClient.newHttpClient();
        var res    = client.send(req, new JsonBodyHandler<>(type));
        return res.body();
    }

    public List<T> readAll(Class<T> type) throws IOException, InterruptedException {
        var req = HttpRequest.newBuilder()
                .uri(mkResource())
                .GET()
                .build();
        var client = HttpClient.newHttpClient();
        var res    = client.send(req, HttpResponse.BodyHandlers.ofString());

        Type listType = new TypeToken<ArrayList<T>>() {
        }.getType();
        return new Gson().fromJson(res.body(), listType);
    }

}
