package mn.persons;
import javax.inject.Singleton;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicInteger;

@Singleton
public class PersonRepo {
    private final AtomicInteger nextId  = new AtomicInteger(1);
    private final List<Person>  persons = new ArrayList<>();

    public PersonRepo() {
        insert("Anna Conda", 20);
        insert("Justin Time", 25);
        insert("Inge Vidare", 30);
        insert("Per Silja", 35);
    }

    public Person insert(String name, int age) {
        var p = new Person(nextId.getAndIncrement(), name, age);
        persons.add(p);
        return p;
    }

    public List<Person> findAll() {
        return persons;
    }

    public Optional<Person> findById(int id) {
        return persons.stream()
                .filter(p -> p.getId() == id)
                .findFirst();
    }

    public void remove(int id) {
        persons.removeIf(p -> p.getId() == id);
    }

    public Optional<Person> update(int id, String name, int age) {
        var maybe = findById(id);
        if (maybe.isEmpty()) return maybe;

        if (name != null) maybe.get().setName(name);
        if (age > 0) maybe.get().setAge(age);

        return maybe;
    }

}

