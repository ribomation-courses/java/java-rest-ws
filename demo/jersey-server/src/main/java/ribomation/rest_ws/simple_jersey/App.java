package ribomation.rest_ws.simple_jersey;

import org.glassfish.grizzly.http.server.HttpServer;
import org.glassfish.jersey.grizzly2.httpserver.GrizzlyHttpServerFactory;
import org.glassfish.jersey.moxy.json.MoxyJsonFeature;
import org.glassfish.jersey.server.ResourceConfig;
import ribomation.rest_ws.simple_jersey.domain.BooksRepo;

import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerResponseContext;
import javax.ws.rs.container.ContainerResponseFilter;
import java.net.URI;
import java.util.logging.Level;
import java.util.logging.Logger;

public class App {
    public static void main(String[] args) throws Exception {
        new App().run();
    }

    void run() throws Exception {
        Logger.getGlobal().setLevel(Level.ALL);
        URI            baseUri = URI.create("http://localhost:9000/");
        Logger         log     = Logger.getLogger(App.class.getName());
        ResourceConfig cfg     = jerseyInit();
        HttpServer     server  = GrizzlyHttpServerFactory.createHttpServer(baseUri, cfg, true);
        Runtime.getRuntime().addShutdownHook(new Thread(server::shutdownNow));

        log.info(String.format("Started: %sbooks%n", baseUri));
        BooksRepo.instance.list().forEach(System.out::println);
        Thread.currentThread().join();
    }

    ResourceConfig jerseyInit() {
        return new ResourceConfig()
                .packages(App.class.getPackage().getName())
                .register(MoxyJsonFeature.class)
                .register(new CrossDomainFilter())
                ;
    }

    public class CrossDomainFilter implements ContainerResponseFilter {
        @Override
        public void filter(ContainerRequestContext creq, ContainerResponseContext cres) {
            cres.getHeaders().add("Access-Control-Allow-Origin", "*");
            cres.getHeaders().add("Access-Control-Allow-Methods", "GET,POST,PUT,DELETE,HEAD,OPTIONS");
        }
    }

}
