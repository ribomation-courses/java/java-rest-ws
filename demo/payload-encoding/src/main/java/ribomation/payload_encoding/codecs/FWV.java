package ribomation.payload_encoding.codecs;

import com.ancientprogramming.fixedformat4j.annotation.Align;
import com.ancientprogramming.fixedformat4j.annotation.Field;
import com.ancientprogramming.fixedformat4j.annotation.FixedFormatBoolean;
import com.ancientprogramming.fixedformat4j.annotation.FixedFormatNumber;
import com.ancientprogramming.fixedformat4j.annotation.FixedFormatPattern;
import com.ancientprogramming.fixedformat4j.annotation.Record;
import com.ancientprogramming.fixedformat4j.format.FixedFormatManager;
import com.ancientprogramming.fixedformat4j.format.impl.FixedFormatManagerImpl;
import ribomation.payload_encoding.Codec;
import ribomation.payload_encoding.domain.Person;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;


public class FWV implements Codec {
    @Record
    public static class AnnotatedPerson extends Person {
        private Person delegate;

        public AnnotatedPerson() { }
        public AnnotatedPerson(Person delegate) {
            this.delegate = delegate;
        }

        public Person getDelegate() {
            return new Person()
                    .name(super.getName())
                    .age(super.getAge())
                    .female(super.isFemale())
                    .lastUpdate(super.getLastUpdate());
        }

        @Override
        @Field(offset = 1, length = 16, paddingChar = '#')
        public String getName() {
            return delegate.getName();
        }

        @Override
        @Field(offset = 17, length = 3, paddingChar = '0', align = Align.RIGHT)
        @FixedFormatNumber()
        public int getAge() {
            return delegate.getAge();
        }

        @Override
        @Field(offset = 20, length = 1)
        @FixedFormatBoolean(trueValue = "F", falseValue = "M")
        public boolean isFemale() {
            return delegate.isFemale();
        }

        @Override
        @Field(offset = 21, length = 19)
        @FixedFormatPattern("yyyy-MM-dd HH:mm:ss")
        public Date getLastUpdate() {
            return delegate.getLastUpdate();
        }
    }

    @Override
    public void encode(List<Person> objs, OutputStream out) {
        FixedFormatManager ffm = new FixedFormatManagerImpl();
        try (PrintWriter writer = new PrintWriter(new OutputStreamWriter(out))) {
            for (Person p : objs) {
                writer.println(ffm.export(new AnnotatedPerson(p)));
            }
        }
    }

    @Override
    public List<Person> decode(InputStream in) {
        FixedFormatManager ffm = new FixedFormatManagerImpl();
        try (BufferedReader reader = new BufferedReader(new InputStreamReader(in))) {
            return reader.lines()
                    .map(line -> ffm.load(AnnotatedPerson.class, line).getDelegate())
                    .collect(Collectors.toList());
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
