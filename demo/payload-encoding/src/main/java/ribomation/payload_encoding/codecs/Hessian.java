package ribomation.payload_encoding.codecs;

import com.caucho.hessian.io.Hessian2Input;
import com.caucho.hessian.io.Hessian2Output;
import ribomation.payload_encoding.Codec;
import ribomation.payload_encoding.domain.Person;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.List;

public class Hessian implements Codec {
    public static class HessianOutClosable extends Hessian2Output implements AutoCloseable {
        public HessianOutClosable(OutputStream os) {
            super(os);
        }
    }
    public static class HessianInClosable extends Hessian2Input implements AutoCloseable {
        public HessianInClosable(InputStream is) {
            super(is);
        }
    }

    @Override
    public boolean isText() {
        return false;
    }

    @Override
    public void encode(List<Person> objs, OutputStream out) {
        try (HessianOutClosable hessian = new HessianOutClosable(out)) {
            hessian.writeObject(objs);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public List<Person> decode(InputStream in) {
        try (HessianInClosable hessian = new HessianInClosable(in)) {
            return (List<Person>) hessian.readObject();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
