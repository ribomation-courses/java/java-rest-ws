package ribomation.payload_encoding;

import ribomation.payload_encoding.domain.Person;

import java.io.InputStream;
import java.io.OutputStream;
import java.util.List;

public interface Codec {
    void encode(List<Person> objs, OutputStream out);
    List<Person> decode(InputStream in);
    default boolean isText() { return true; }
}
