package ribomation.java_rest_ws.numbers;

import io.micronaut.http.annotation.Controller;
import io.micronaut.http.annotation.Get;
import io.micronaut.http.HttpStatus;

import java.math.BigInteger;

@Controller("/numbers")
public class NumbersController {

    @Get("/sum/:arg")
    public NumberData sum(int arg) {
        var startTime   = System.nanoTime();
        var result      = arg * (arg + 1) / 2;
        var endTime     = System.nanoTime();
        var elapsedTime = (endTime - startTime) * 1E-9;

        return new NumberData("sum", arg, BigInteger.valueOf(result), elapsedTime);
    }


}


