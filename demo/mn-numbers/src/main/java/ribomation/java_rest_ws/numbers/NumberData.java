package ribomation.java_rest_ws.numbers;

import java.math.BigInteger;

public class NumberData {
    String     function;
    int        argument;
    BigInteger value;
    double     elapsedTimeInSeconds;

    public NumberData() {
    }

    public NumberData(String function, int argument, BigInteger value, double elapsedTimeInSeconds) {
        this.function = function;
        this.argument = argument;
        this.value = value;
        this.elapsedTimeInSeconds = elapsedTimeInSeconds;
    }
}
