package ribomation.java_rest_ws.echo;

import io.micronaut.http.MediaType;
import io.micronaut.http.annotation.Controller;
import io.micronaut.http.annotation.Get;
import io.micronaut.http.annotation.Header;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Optional;



@Controller("/echo")
public class EchoController {
    private static final Logger log = LoggerFactory.getLogger("echo");

    @Get(value="/{payload}", produces = MediaType.TEXT_PLAIN)
    public String index(
            String payload,
            @Header String userAgent,
            @Header Optional<Integer> contentLength)
    {
        log.info("User-Agent: {}", userAgent);
        log.info("Content-Length: {}", contentLength.orElse(-1));
        if (payload == null || payload.trim().length() == 0) return "[empty]";
        return payload.toUpperCase().repeat(3);
    }
}

