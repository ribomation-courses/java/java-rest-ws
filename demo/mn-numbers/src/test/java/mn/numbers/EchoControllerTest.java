package mn.numbers;

import io.micronaut.context.ApplicationContext;
import io.micronaut.core.type.Argument;
import io.micronaut.http.HttpRequest;
import io.micronaut.http.client.HttpClient;
import io.micronaut.runtime.server.EmbeddedServer;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

public class EchoControllerTest {
    private static EmbeddedServer server;
    private static HttpClient     client;

    @BeforeAll
    public static void launchServer() {
        server = ApplicationContext.run(EmbeddedServer.class);
        client = server.getApplicationContext().createBean(HttpClient.class, server.getURL());
    }

    @AfterAll
    public static void shutdownServer() {
        if (server != null) server.stop();
        if (client != null) client.stop();
    }

    @Test
    @DisplayName("echo/a --> AAA")
    public void simple_echo() {
        var req = HttpRequest
                .GET("/echo/a")
                .header("User-Agent", "JUnit/5");
        var result = client.toBlocking()
                .retrieve(req, Argument.of(String.class));
        assertNotNull(result);
        assertEquals("AAA", result);
    }

    @Test
    @DisplayName("echo/hi --> HIHIHI")
    public void simple_echo2() {
        var req = HttpRequest
                .GET("/echo/hi")
                .header("User-Agent", "JUnit/5")
                .header("Content-Length", "42");
        var res = client.toBlocking()
                .exchange(req, Argument.of(String.class));
        assertNotNull(res);
        assertEquals(200, res.status().getCode());
        assertEquals("HIHIHI", res.body());
    }
}


