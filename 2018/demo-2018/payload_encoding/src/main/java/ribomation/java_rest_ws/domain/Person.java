package ribomation.java_rest_ws.domain;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Objects;

public class Person implements Serializable {
    private String  name;
    private int     age;
    private boolean female;
    private Date    lastUpdate;

    @Override
    public String toString() {
        return "Person{" +
                "name='" + name + '\'' +
                ", age=" + age +
                ", female=" + female +
                ", lastUpdate=" + new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(lastUpdate) +
                '}';
    }
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Person person = (Person) o;
        return age == person.age &&
                female == person.female &&
                Objects.equals(name, person.name) ;
    }
    @Override
    public int hashCode() {
        return Objects.hash(name, age, female);
    }

    public String getName() {
        return name;
    }
    public Person name(String name) {
        this.name = name;
        return this;
    }
    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }
    public Person age(int age) {
        this.age = age;
        return this;
    }
    public void setAge(int age) {
        this.age = age;
    }

    public boolean isFemale() {
        return female;
    }
    public Person female(boolean female) {
        this.female = female;
        return this;
    }
    public void setFemale(boolean female) {
        this.female = female;
    }

    public Date getLastUpdate() {
        return lastUpdate;
    }
    public Person lastUpdate(Date lastUpdate) {
        this.lastUpdate = lastUpdate;
        return this;
    }
    public void setLastUpdate(Date lastUpdate) {
        this.lastUpdate = lastUpdate;
    }
}
