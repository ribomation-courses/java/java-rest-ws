package ribomation.java_rest_ws.codecs;

import ribomation.java_rest_ws.Codec;
import ribomation.java_rest_ws.domain.Person;

import java.beans.XMLDecoder;
import java.beans.XMLEncoder;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.List;


public class XML implements Codec {
    @Override
    public void encode(List<Person> objs, OutputStream out) {
        try (XMLEncoder encoder = new XMLEncoder(out)) {
            encoder.writeObject(objs);
        }
    }

    @Override
    public List<Person> decode(InputStream in) {
        try (XMLDecoder decoder = new XMLDecoder(in)) {
            return (List<Person>) decoder.readObject();
        }
    }
}
