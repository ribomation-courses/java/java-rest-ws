package ribomation.rest_ws.simple_jersey;

import io.swagger.jaxrs.config.BeanConfig;
import org.glassfish.jersey.moxy.json.MoxyJsonFeature;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;
import java.net.URI;
import java.util.HashSet;
import java.util.Set;

@ApplicationPath("/")
public class App extends Application {

    public App() {
        URI baseUri = URI.create("http://localhost:9000/app");
        swaggerInit(baseUri);
        BooksRepo.instance.populate();
    }

    @Override
    public Set<Class<?>> getClasses() {
        final Set<Class<?>> classes = new HashSet<>();
        classes.add(BookResource.class);
        classes.add(MoxyJsonFeature.class);
        classes.add(io.swagger.jaxrs.listing.ApiListingResource.class);
        classes.add(io.swagger.jaxrs.listing.SwaggerSerializers.class);
        return classes;
    }

    private void swaggerInit(URI uri) {
        BeanConfig beanConfig = new BeanConfig();
        beanConfig.setVersion("0.42");
        beanConfig.setSchemes(new String[]{"http"});
        beanConfig.setHost(String.format("%s:%d", uri.getHost(), uri.getPort()));
        beanConfig.setBasePath(uri.getPath());
        beanConfig.setResourcePackage(App.class.getPackage().getName());
        beanConfig.setPrettyPrint(true);
        beanConfig.setScan(true);
    }

}
