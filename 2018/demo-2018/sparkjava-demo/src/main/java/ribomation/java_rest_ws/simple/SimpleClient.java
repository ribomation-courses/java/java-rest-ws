package ribomation.java_rest_ws.simple;
import java.io.IOException;
import java.net.URL;

public class SimpleClient {
    public static void main(String[] args) throws Exception {
        String url = "https://www.ribomation.se/pages/contact.html";
        new SimpleClient().invoke(url);
    }

     void invoke(String url) throws IOException {
         new URL(url).openStream().transferTo(System.out);
    }
}
