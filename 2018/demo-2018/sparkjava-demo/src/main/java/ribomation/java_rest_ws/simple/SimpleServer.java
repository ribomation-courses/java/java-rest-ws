package ribomation.java_rest_ws.simple;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;

import static spark.Spark.*;

public class SimpleServer {
    public static void main(String[] args) {
        port(8888);

        notFound((req, res) -> {
            res.type("text/plain");
            return String.format("URI: %s", req.url());
        });

        get("/txt", (req, res) -> {
            res.type("text/plain");
            return richardIII;
        });

        get("/fib/:arg", (req, res) -> {
            int  n      = Integer.parseInt(req.params("arg"));
            long result = fib(n);
            res.type("text/plain");
            return String.format("fib(%d) = %d", n, result);
        });
    }

    private static long fib(int n) {
        return n <= 2 ? 1 : fib(n - 2) + fib(n - 1);
    }

    private static final String richardIII;
    static {
        String      name = "/Richard-III.txt";
        InputStream is   = SimpleServer.class.getResourceAsStream(name);
        if (is == null) {
            throw new RuntimeException("cannot find resource: " + name);
        }
        ByteArrayOutputStream buf = new ByteArrayOutputStream();
        try {
            is.transferTo(buf);
            richardIII = buf.toString();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}

