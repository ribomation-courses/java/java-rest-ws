package ribomation.java_rest_ws.json;

import com.google.gson.Gson;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import spark.Request;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

import static spark.Spark.*;

public class JsonServer {
    public static void main(String[] args) {
        port(8888);

        final Logger LOG = LoggerFactory.getLogger(JsonServer.class);
        notFound((req, res) -> {
            res.status(404);
            return null;
        });
        exception(IndexOutOfBoundsException.class, (ex, req, res) -> {
            LOG.warn("ERR: {}", ex.toString());
            res.status(404);
        });
        exception(NumberFormatException.class, (ex, req, res) -> {
            LOG.warn("ERR: {}", ex.toString());
            res.status(404);
        });

        final String JSON = "application/json";
        final Gson   gson = new Gson();
        after((req, res) -> {
            res.type(JSON);
        });

        List<Person> persons = new ArrayList<>();
        persons.add(new Person("Anna Conda", 42));
        persons.add(new Person("Per Silja", 32));
        persons.add(new Person("Justin Time", 27));
        persons.add(new Person("Sham Poo", 51));

        get("/persons", (req, res) -> persons, gson::toJson);

        post("/persons", JSON, (req, res) -> {
            LOG.info("POST");
            Person p = gson.fromJson(req.body(), Person.class);
            if (p == null) halt(400);
            LOG.info("POST {}", p);
            persons.add(p.ID());
            res.status(201);
            return p;
        }, gson::toJson);

        get("/person/:id", (req, res) -> {
            LOG.info("GET id");
            Optional<Person> P = find(req, persons);
            if (P.isPresent()) {
                LOG.info("GET {}", P.get());
                return P.get();
            } else {
                halt(404);
                return null;
            }
        }, gson::toJson);

        put("/person/:id", "application/json", (req, res) -> {
            LOG.info("PUT");
            Optional<Person> P = find(req, persons);
            if (P.isPresent()) {
                Person p = P.get();
                LOG.info("PUT {}", p);
                Person p2 = gson.fromJson(req.body(), Person.class);
                if (p2.name != null) p.name = p2.name;
                if (p2.age > 0) p.age = p2.age;
                return p;
            } else {
                halt(404);
                return null;
            }
        }, gson::toJson);

        delete("/person/:id", "application/json", (req, res) -> {
            find(req, persons)
                    .ifPresentOrElse(p -> {
                        LOG.info("DELETE {}", p);
                        persons.remove(p);
                    }, () -> halt(404));
            res.status(204);
            return null;
        }, gson::toJson);
    }

    static Optional<Person> find(Request r, List<Person> persons) {
        int id = Integer.parseInt(r.params("id"));
        return persons.stream()
                .filter(p -> p.id == id)
                .findFirst();
    }

    static class Person {
        static int nextId = 1;
        int id = -1;
        String name;
        int    age;

        Person() { }
        Person(String name, int age) {
            this(nextId++, name, age);
        }
        Person(int id, String name, int age) {
            this.id = id;
            this.name = name;
            this.age = age;
        }
        Person ID() {
            if (id < 1) id = nextId++;
            return this;
        }

        @Override
        public String toString() {
            return String.format("Person{ID=%d, name=%s, age=%d}", id, name, age);
        }
        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            Person person = (Person) o;
            return id == person.id &&
                    age == person.age &&
                    Objects.equals(name, person.name);
        }
        @Override
        public int hashCode() {
            return Objects.hash(id, name, age);
        }
    }
}
