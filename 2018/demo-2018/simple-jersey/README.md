Simple Jersey
====

Using Gradle Wrapper
----

### Windows
    gradlew.bat <task>
### *NIX
    gradlew <task>

Launch rest-ws app
----
    gradlew run  

Invoke HTTPie, with CRUD ops
----
    http :9000/books
    http post   :9000/books title=TTT author=AAA price=PPP
    http get    :9000/books/1
    http put    :9000/books/1 title=TTT
    http delete :9000/books/1 

Run unit tests
----
    gradlew test
Test results report in

    build/reports/tests/test/index.html

