package ribomation.rest_ws.simple_jersey;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.List;

@Path("/books")
@Consumes(MediaType.APPLICATION_JSON)
@Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
public class BookResource {
    @GET
    public List<Book> allBooks() {
        return BooksRepo.instance.list();
    }

    @GET
    @Path("{id}")
    public Book getBook(@PathParam("id") long id) {
        Book book = BooksRepo.instance.get(id);
        if (book == null) throw new NotFoundException();
        return book;
    }

    @POST
    public Book createBook(Book book) {
        return BooksRepo.instance.create(book);
    }

    @PUT
    @Path("{id}")
    public Book updateBook(@PathParam("id") long id, Book delta) {
        Book book = BooksRepo.instance.update(id, delta);
        if (book == null) throw new NotFoundException();
        return book;
    }

    @DELETE
    @Path("{id}")
    public void delete(@PathParam("id") long id) {
        if (!BooksRepo.instance.delete(id)) {
            throw new NotFoundException();
        }
    }
}
