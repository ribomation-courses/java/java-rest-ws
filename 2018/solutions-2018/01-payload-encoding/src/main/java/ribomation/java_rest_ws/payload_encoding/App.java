package ribomation.java_rest_ws.payload_encoding;

import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import java.beans.XMLEncoder;
import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class App {
    public static void main(String[] args) {
        App app = new App();
        app.run();
    }

    void run() {
        List<Product> products = load("/products.csv");
        String        xml      = toXML(products);
        String        json     = toJSON(products);
        System.out.printf("--- XML ---%n%s%n", xml);
        System.out.printf("--- JSON ---%n%s%n", json);

        System.out.println("--- Payload Size ---");
        System.out.printf("XML : %,d%n", xml.length());
        System.out.printf("JSON: %,d%n", json.length());
    }

    List<Product> load(String resource) {
        InputStream    is = getClass().getResourceAsStream(resource);
        BufferedReader br = new BufferedReader(new InputStreamReader(is));
        return br.lines()
                .skip(1)
                .map(Product::fromCSV)
                .collect(Collectors.toList())
                ;
    }

    String toXML(List<Product> products) {
        ByteArrayOutputStream buf     = new ByteArrayOutputStream();
        XMLEncoder            encoder = new XMLEncoder(buf);
        encoder.writeObject(products);
        encoder.close();
        return buf.toString();
    }

    String toJSON(List<Product> products) {
        return new GsonBuilder()
                .setPrettyPrinting()
                .setDateFormat("yyyy-MM-dd-HH-mm-ss")
                .create()
                .toJson(products,
                        new TypeToken<ArrayList<Product>>() {
                        }.getType());
    }

}
