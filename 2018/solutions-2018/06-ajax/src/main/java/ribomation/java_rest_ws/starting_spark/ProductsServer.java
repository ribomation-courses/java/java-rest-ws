package ribomation.java_rest_ws.starting_spark;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicLong;
import java.util.stream.Collectors;

import static spark.Spark.*;

public class ProductsServer {
    final String     JSON   = "application/json";
    final AtomicLong nextId = new AtomicLong(0);
    final Gson   gson;
    final Logger LOG;

    public static void main(String[] args) throws Exception {
        ProductsServer app = new ProductsServer();
        app.run();
    }

    ProductsServer() {
        System.setProperty("org.slf4j.simpleLogger.showShortLogName", "true");
        System.setProperty("org.slf4j.simpleLogger.showThreadName", "false");
        LOG = LoggerFactory.getLogger(getClass());
        gson = new GsonBuilder()
                .setDateFormat("yyyy-MM-dd-HH-mm-ss")
                .create();
    }

    void run() throws Exception {
        final int     port     = 9000;
        List<Product> products = load("/products.csv");
        setup(port);
        routes(products);
        LOG.info("URI: http://localhost:{}/products", port);
        LOG.info("URI: http://localhost:{}/product/:ID", port);
    }

    void routes(List<Product> products) {
        get("/products", (req, res) -> products, gson::toJson);

        post("/products", JSON, (req, res) -> {
            Product p = gson.fromJson(req.body(), Product.class);
            if (p == null) halt(400);

            LOG.info("PST {}", p);
            p.setId(nextId.incrementAndGet());
            p.setModified(new Date());
            products.add(p);

            res.status(201);
            return p;
        }, gson::toJson);

        get("/product/:id",
                (req, res) -> findById(req.params("id"), products)
                        .orElseGet(() -> {
                            halt(404);
                            return null;
                        }), gson::toJson);

        put("/product/:id", JSON, (req, res) -> {
            Optional<Product> P = findById(req.params("id"), products);
            if (P.isPresent()) {
                Product p = gson.fromJson(req.body(), Product.class);
                LOG.info("PUT {}", p);
                return P.get().update(p);
            } else {
                res.status(404);
                return null;
            }
        }, gson::toJson);

        delete("/product/:id", JSON, (req, res) -> {
            Optional<Product> P = findById(req.params("id"), products);
            if (P.isPresent()) {
                Product p = P.get();
                LOG.info("DEL {}", p);
                products.remove(p);
                res.status(204);
            } else {
                res.status(404);
            }
            return null;
        }, gson::toJson);
    }

    Optional<Product> findById(String id, List<Product> products) {
        return findById(Integer.parseInt(id), products);
    }

    Optional<Product> findById(long id, List<Product> products) {
        return products.stream()
                .filter(p -> p.getId() == id)
                .findFirst();
    }

    List<Product> load(String resource) {
        InputStream    is = getClass().getResourceAsStream(resource);
        BufferedReader br = new BufferedReader(new InputStreamReader(is));
        return br.lines()
                .skip(1)
                .map(Product::fromCSV)
                .peek(p -> p.setId(nextId.incrementAndGet()))
                .collect(Collectors.toList())
                ;
    }

    void setup(int port) {
        port(port);
        staticFiles.location("/assets");
        notFound((req, res) -> {
            res.status(404);
            return null;
        });
        exception(IndexOutOfBoundsException.class, (ex, req, res) -> {
            LOG.warn("ERR: {}", ex.toString());
            res.status(404);
        });
        exception(NumberFormatException.class, (ex, req, res) -> {
            LOG.warn("ERR: {}", ex.toString());
            res.status(404);
        });
        before((req, res) -> {
            LOG.info("REQ: {} {}", req.requestMethod(), req.uri());
        });
        after((req, res) -> {
            res.type(JSON);
        });
    }
}
