package ribomation.java_rest_ws.starting_spark;

public class Login {
    String username;
    String password;

    public Login() {
    }

    public Login(String username, String password) {
        this.username = username;
        this.password = password;
    }

    public boolean hasData() {
        return d(username) && d(password);
    }

    private boolean d(String s) {
        return s != null && s.trim().length() > 0;
    }
}
