package ribomation.java_rest_ws.starting_spark;


public class JwtData {
    String username;
    String jwt;

    public JwtData() {
    }

    public JwtData(String username, String jwt) {
        this.username = username;
        this.jwt = jwt;
    }
}
