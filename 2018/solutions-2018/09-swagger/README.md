Simple Jersey-Swagger
====

Using Gradle Wrapper
----

### Windows
    gradlew.bat <task>
### *NIX
    gradlew <task>

Launch rest-ws app
----
    gradlew appRun 

Open a browser to
* [http://localhost:9000/](http://localhost:9000/)
for the top-most page 
* [http://localhost:9000/docs//](http://localhost:9000/docs/)
to view the Swagger docs.

Invoke HTTPie, with CRUD ops
----
    http :9000/app/books
    http post   :9000/app/books title=TTT author=AAA price=PPP
    http get    :9000/app/books/1
    http put    :9000/app/books/1 title=TTT
    http delete :9000/app/books/1 

Invoke HTTPie, fetch specs
----
    http :9000/app/swagger.json
    http :9000/app/swagger.yaml
    http :9000/app/application.wadl

Generate a deployable WAR
----
    gradlew build
The WAR file then be found in

    ./build/libs/*.war

Run from a generated WAR
----
    gradlew appRunWar

Run unit tests
----
    gradlew test
Test results report in

    ./build/reports/tests/test/index.html

