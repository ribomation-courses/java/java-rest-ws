package ribomation.rest_ws.simple_jersey;

import io.swagger.jaxrs.config.BeanConfig;
import io.swagger.models.Contact;
import io.swagger.models.Info;
import org.glassfish.jersey.moxy.json.MoxyJsonFeature;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;
import java.net.URI;
import java.util.HashSet;
import java.util.Set;

//@ApplicationPath("api/v1")
public class App extends Application {

    public App() {
        URI baseUri = URI.create("http://localhost:9000/api/v1");
        swaggerInit(baseUri);
        ProductsRepo.instance.load();
    }

    @Override
    public Set<Class<?>> getClasses() {
        final Set<Class<?>> classes = new HashSet<>();
        classes.add(ProductResource.class);
        classes.add(MoxyJsonFeature.class);
        classes.add(io.swagger.jaxrs.listing.ApiListingResource.class);
        classes.add(io.swagger.jaxrs.listing.SwaggerSerializers.class);
        return classes;
    }

    private void swaggerInit(URI uri) {
        BeanConfig cfg = new BeanConfig();

        cfg.setSchemes(new String[]{uri.getScheme()});
        cfg.setHost(String.format("%s:%d", uri.getHost(), uri.getPort()));
        cfg.setBasePath(uri.getPath());

        cfg.setResourcePackage(App.class.getPackage().getName());

        cfg.getSwagger().info(new Info()
                .title("Fake Products")
                .description("A JAX-RS Jersey-Swagger Demo web-app.")
                .version("0.42")
                .contact(new Contact()
                        .name("Ribomation")
                        .url("https://www.ribomation.se/")
                        .email("info@ribomation.se")
                )
        );

        cfg.setPrettyPrint(true);
        cfg.setScan();
    }

}
