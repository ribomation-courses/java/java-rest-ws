@echo on

@echo --- Creating a new product ---
http :9000/products "name=Java REST-WS Primer" price=512 items=42

@echo --- Listing a single product ---
http :9000/products/101

@echo --- Updating a single product ---
http put :9000/products/101 price=256 items=12

@echo --- Deleting a single product ---
http delete :9000/products/101

@echo --- Listing a deleted product ---
http :9000/products/101

@echo --- Performing unknown URI ---
http get :9000/whatever

@echo --- Performing illegal OP ---
http get :9000/products/123456

