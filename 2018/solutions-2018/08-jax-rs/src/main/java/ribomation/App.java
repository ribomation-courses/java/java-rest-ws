package ribomation;

import org.glassfish.grizzly.http.server.HttpServer;
import org.glassfish.jersey.grizzly2.httpserver.GrizzlyHttpServerFactory;
import org.glassfish.jersey.moxy.json.MoxyJsonFeature;
import org.glassfish.jersey.server.ResourceConfig;

import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerResponseContext;
import javax.ws.rs.container.ContainerResponseFilter;
import java.net.URI;
import java.util.logging.Level;
import java.util.logging.Logger;

public class App {
    public static void main(String[] args) throws Exception {
        App app = new App();
        app.setup();
        app.run();
    }

    final Logger log = Logger.getLogger(App.class.getName());

    void setup() {
        Logger.getGlobal().setLevel(Level.ALL);
        ProductsRepo.instance.load();
    }

    void run() throws InterruptedException {
        URI baseUri = URI.create("http://localhost:9000/");
        ResourceConfig cfg = jerseyInit();
        HttpServer server = GrizzlyHttpServerFactory.createHttpServer(baseUri, cfg, true);
        Runtime.getRuntime().addShutdownHook(new Thread(server::shutdownNow));
        log.info(String.format("Started: %s%n", baseUri));
        Thread.currentThread().join();
    }

    ResourceConfig jerseyInit() {
        return new ResourceConfig()
                .packages(App.class.getPackage().getName())
                .register(MoxyJsonFeature.class)
                .register(new CrossDomainFilter())
                ;
    }

    public class CrossDomainFilter implements ContainerResponseFilter {
        @Override
        public void filter(ContainerRequestContext creq, ContainerResponseContext cres) {
            cres.getHeaders().add("Access-Control-Allow-Origin", "*");
            cres.getHeaders().add("Access-Control-Allow-Methods", "GET,POST,PUT,DELETE,HEAD,OPTIONS");
        }
    }
}
