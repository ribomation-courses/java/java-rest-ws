package ribomation;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.*;

public class ProductsRepo {
    public static final ProductsRepo instance = new ProductsRepo();

    private ProductsRepo() {
    }

    private Map<Long, Product> repo;

    public void load() {
        repo = Collections.synchronizedMap(load("/products.csv"));
    }

    public List<Product> list() {
        return new ArrayList<Product>(repo.values());
    }

    public Product get(long id) {
        return repo.get(id);
    }

    public Product add(Product product) {
        product = product.assignId();
        repo.put(product.getId(), product);
        return product;
    }

    public boolean remove(long id) {
        return repo.remove(id) != null;
    }

    private Map<Long, Product> load(String path) {
        InputStream is = this.getClass().getResourceAsStream(path);
        if (is == null) {
            throw new RuntimeException("cannot open " + path);
        }
        Map<Long, Product> products = new TreeMap<>();
       new BufferedReader(new InputStreamReader(is))
                .lines()
               .skip(1)
                .map(Product::fromCSV)
                .forEach(p -> {
                    products.put(p.getId(), p);
                });
        return products;
    }

    public static void main(String[] args) {
        System.out.println(new ProductsRepo().load("/products.csv"));
    }
}
