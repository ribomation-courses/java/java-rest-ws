package ribomation;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Objects;
import java.util.concurrent.atomic.AtomicLong;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class Product {
    private static AtomicLong nextId = new AtomicLong(1);
    private long id;
    private String name;
    private float price;
    private int items;
    private Date modified;

    public static Product fromCSV(String csv) {
        return fromCSV(csv, ";");
    }

    public static Product fromCSV(String csv, String delim) {
        try {
            String[] fields = csv.split(delim);
            return new Product(fields[0], fields[1], fields[2], fields[3]);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public Product() {
    }

    public Product(String name, String price, String items, String modified) throws Exception {
        //name,price,items,modified
        //"Basil - Primerba, Paste",619.19,12,2017-08-29 16:24:45
        this(nextId.getAndIncrement(),
                name.replaceAll("\"", ""),
                Float.parseFloat(price),
                Integer.parseInt(items),
                new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(modified));
    }

    public Product(long id, String name, float price, int items, Date modified) {
        this.id = id;
        this.name = name;
        this.price = price;
        this.items = items;
        this.modified = modified;
    }

    Product assignId() {
        this.id = nextId.getAndIncrement();
        return this;
    }

    @Override
    public String toString() {
        return "Product{" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                ", price=" + price +
                ", items=" + items +
                ", modified=" + modified +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Product product = (Product) o;
        return getId() == product.getId() &&
                Float.compare(product.getPrice(), getPrice()) == 0 &&
                getItems() == product.getItems() &&
                Objects.equals(getName(), product.getName()) &&
                Objects.equals(getModified(), product.getModified());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getName(), getPrice(), getItems(), getModified());
    }

    public long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public float getPrice() {
        return price;
    }

    public int getItems() {
        return items;
    }

    public Date getModified() {
        return modified;
    }

    public void setId(long id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setPrice(float price) {
        this.price = price;
    }

    public void setItems(int items) {
        this.items = items;
    }

    public void setModified(Date modified) {
        this.modified = modified;
    }
}
