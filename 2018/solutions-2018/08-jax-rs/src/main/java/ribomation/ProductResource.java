package ribomation;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.List;

@Path("/products")
@Consumes(MediaType.APPLICATION_JSON)
@Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
public class ProductResource {
    @GET
    public List<Product> allProduct() {
        return ProductsRepo.instance.list();
    }

    @POST
    public Product createBook(Product product) {
        return ProductsRepo.instance.add(product);
    }

    @GET
    @Path("{id}")
    public Product getProduct(@PathParam("id") long id) {
        Product product = ProductsRepo.instance.get(id);
        if (product == null) throw new NotFoundException();
        return product;
    }

    @PUT
    @Path("{id}")
    public Product updateProduct(@PathParam("id") long id, Product delta) {
        Product product = ProductsRepo.instance.get(id);
        if (product == null) throw new NotFoundException();

        if (delta.getName() != null) {
            product.setName(delta.getName());
        }
        if (delta.getPrice() > 0) {
            product.setPrice(delta.getPrice());
        }
        if (delta.getItems() > 0) {
            product.setItems(delta.getItems());
        }

        return product;
    }

    @DELETE
    @Path("{id}")
    public void delete(@PathParam("id") long id) {
        Product product = ProductsRepo.instance.get(id);
        if (product == null) throw new NotFoundException();

        ProductsRepo.instance.remove(id);
    }

}
