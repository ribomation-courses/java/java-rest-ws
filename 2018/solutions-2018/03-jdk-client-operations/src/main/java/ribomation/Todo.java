package ribomation;

import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

// {
//    "userId": 1,
//    "id": 8,
//    "title": "quo adipisci enim quam ut ab",
//    "completed": true
// }
public class Todo {
    Integer userId;
    Integer id;
    String title;
    Boolean completed;

    public static Todo fromJson(String json) {
        return new GsonBuilder()
                .create()
                .fromJson(json, Todo.class);
    }

    public static List<Todo> fromJsonList(String json) {
        final Type TYPE = new TypeToken<ArrayList<Todo>>() {}.getType();
        return new GsonBuilder()
                .create()
                .fromJson(json, TYPE);
    }

    public String toJson() {
        return new GsonBuilder()
                .setPrettyPrinting()
                .create()
                .toJson(this);
    }

    public Todo() {
    }

    public Todo(Integer userId, Integer id, String title, Boolean completed) {
        this.userId = userId;
        this.id = id;
        this.title = title;
        this.completed = completed;
    }

    @Override
    public String toString() {
        return "Todo{" +
                "userId=" + userId +
                ", id=" + id +
                ", title='" + title + '\'' +
                ", completed=" + completed +
                '}';
    }

}
