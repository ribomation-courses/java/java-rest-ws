package ribomation;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;
import java.util.stream.Collectors;

public class JsonPlaceHolderClient {
    public static void main(String[] args) throws IOException {
        JsonPlaceHolderClient app = new JsonPlaceHolderClient();
        app.run("https://jsonplaceholder.typicode.com");
    }

    void run(String baseUri) throws IOException {
        List<Todo> todos = list(baseUri);
        todos.forEach(System.out::println);

        System.out.println("----------------");
        final int id = 8;
        Todo todo = get(baseUri, id);
        System.out.printf("TODO: %s%n", todo);

        System.out.println("----------------");
        todo = new Todo(42, -1, "Just a test", false);
        todo = create(baseUri, todo);
        System.out.printf("CREATED: %s%n", todo);
    }

    Todo create(String baseUri, Todo todo) throws IOException {
        HttpURLConnection con = open(resource(baseUri, "todos"), "POST");
        send(todo, con.getOutputStream());
        return Todo.fromJson(recv(con.getInputStream()));
    }

     void send(Todo todo, OutputStream os) throws IOException {
         OutputStreamWriter w = new OutputStreamWriter(os, "US-ASCII");
         w.write(todo.toJson());
         w.flush();
         w.close();
    }


    Todo get(String baseUri, int id) throws IOException {
        HttpURLConnection con = open(resource(baseUri, "todos", id), "GET");
        return Todo.fromJson(recv(con.getInputStream()));
    }

    List<Todo> list(String baseUri) throws IOException {
        HttpURLConnection con = open(resource(baseUri, "todos"), "GET");
        return Todo.fromJsonList(recv(con.getInputStream()));
    }

    String recv(InputStream is) {
        BufferedReader in = new BufferedReader(new InputStreamReader(is));
        return in.lines().collect(Collectors.joining());
    }


    HttpURLConnection open(URL url, String op) throws IOException {
        HttpURLConnection con = (HttpURLConnection) url.openConnection();
        con.setRequestMethod(op);
        con.setDoOutput(!(op.equals("GET") || op.equals("DELETE")));
        con.setDoInput(true);
        con.setConnectTimeout(10 * 1000);
        con.setRequestProperty("Accept", "application/json");
        con.setInstanceFollowRedirects(true);
        con.connect();
        return con;
    }

    URL resource(String baseUri, String resource) throws MalformedURLException {
        return new URL(String.format("%s/%s", baseUri, resource));
    }

    URL resource(String baseUri, String resource, int id) throws MalformedURLException {
        return new URL(String.format("%s/%s/%d", baseUri, resource, id));
    }


}
