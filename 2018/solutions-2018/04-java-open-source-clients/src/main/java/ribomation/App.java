package ribomation;

import com.goebl.david.Webb;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

public class App {
    public static void main(String[] args) throws Exception {
        App app = new App();
        app.run("https://jsonplaceholder.typicode.com");
    }

    void run(String baseUri) throws Exception {
        System.out.println("--- ALL --------");
        List<Todo> all = all(baseUri);
        all.forEach(System.out::println);

        System.out.println("--- CREATE --------");
        Todo todo = create(baseUri, new Todo(42, -1, "Learn REST WS", false));
        todo.setId(8);
        System.out.printf("TODO: %s%n", todo);

        System.out.println("--- READ --------");
        todo = get(baseUri, todo.getId());
        System.out.printf("TODO: %s%n", todo);

        System.out.println("--- UPDATE --------");
        todo = update(baseUri, todo.getId(), new Todo(42, -1, "Learn REST WS", false));
        System.out.printf("TODO: %s%n", todo);

        System.out.println("--- DELETE --------");
        delete(baseUri, todo.getId());
        System.out.printf("TODO: %s%n", todo);

        System.out.println("--- READ (should fail) --------");
        todo = get(baseUri, todo.getId());
        System.out.printf("TODO: %s%n", todo);
    }


    Todo get(String baseUri, int id) {
        String uri = String.format("%s/todos/%d", baseUri, id);
        JSONObject jsonObject = Webb.create()
                .get(uri)
                .ensureSuccess()
                .asJsonObject()
                .getBody();
        return Todo.fromJson(jsonObject);
    }

    List<Todo> all(String baseUri) throws JSONException {
        String uri = String.format("%s/todos", baseUri);
        JSONArray jsonArray = Webb.create()
                .get(uri)
                .ensureSuccess()
                .asJsonArray()
                .getBody();
        final int N = jsonArray.length();
        ArrayList<Todo> todos = new ArrayList<>();
        for (int k = 0; k < N; ++k) todos.add(Todo.fromJson(jsonArray.getJSONObject(k)));
        return todos;
    }

    Todo create(String baseUri, Todo todo) {
        String uri = String.format("%s/todos", baseUri);
        JSONObject jsonObject = Webb.create()
                .post(uri)
                .body(todo.toJson())
                .ensureSuccess()
                .asJsonObject()
                .getBody();
        return Todo.fromJson(jsonObject);
    }

    Todo update(String baseUri, int id, Todo todo) {
        String uri = String.format("%s/todos/%d", baseUri, id);
        JSONObject jsonObject = Webb.create()
                .put(uri)
                .body(todo.toJson())
                .ensureSuccess()
                .asJsonObject()
                .getBody();
        return Todo.fromJson(jsonObject);
    }

    void delete(String baseUri, int id) {
        String uri = String.format("%s/todos/%d", baseUri, id);
        Webb.create()
                .delete(uri)
                .ensureSuccess()
                .asVoid();
    }

}
