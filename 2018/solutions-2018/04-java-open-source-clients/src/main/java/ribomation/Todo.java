package ribomation;

import org.json.JSONException;
import org.json.JSONObject;

// {
//    "userId": 1,
//    "id": 8,
//    "title": "quo adipisci enim quam ut ab",
//    "completed": true
// }
public class Todo {
    Integer userId;
    Integer id;
    String title;
    Boolean completed;

    public static Todo fromJson(JSONObject json) {
        Todo todo=new Todo();
        try {
            todo.userId = json.getInt("userId");
            todo.id = json.getInt("id");
            todo.title = json.getString("title");
            todo.completed = json.getBoolean("completed");
            return todo;
        } catch (JSONException e) {
            throw new RuntimeException(e);
        }
    }

    public JSONObject toJson() {
        try {
            return new JSONObject()
                    .put("userId", getUserId())
                    .put("id", getId())
                    .put("title", getTitle())
                    .put("completed", getCompleted());
        } catch (JSONException e) {
            throw new RuntimeException(e);
        }
    }


    public Todo() {
    }

    public Todo(Integer userId, Integer id, String title, Boolean completed) {
        this.userId = userId;
        this.id = id;
        this.title = title;
        this.completed = completed;
    }

    @Override
    public String toString() {
        return "Todo{" +
                "userId=" + userId +
                ", id=" + id +
                ", title='" + title + '\'' +
                ", completed=" + completed +
                '}';
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Boolean getCompleted() {
        return completed;
    }

    public void setCompleted(Boolean completed) {
        this.completed = completed;
    }
}
