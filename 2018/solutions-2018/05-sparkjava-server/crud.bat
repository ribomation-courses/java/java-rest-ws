@echo on

@echo --- Creating a new product ---
http :8888/products "name=Java REST-WS Primer" price=512 items=42

@echo --- Listing a single product ---
http :8888/product/101

@echo --- Updating a single product ---
http put :8888/product/101 price=256 items=12

@echo --- Deleting a single product ---
http delete :8888/product/101

@echo --- Listing a deleted product ---
http :8888/product/101

@echo --- Performing unknown URI ---
http :8888/whatever

@echo --- Performing illegal OP ---
http get :8888/products/42



