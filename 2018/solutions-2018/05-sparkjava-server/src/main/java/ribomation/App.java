package ribomation;

import com.google.gson.Gson;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static spark.Spark.*;

public class App {
    public static void main(String[] args) {
        App app = new App();
        app.loadProducts("/products.csv");
        app.config();
        app.routes();
    }

    final String JSON = "application/json";
    final Logger log = LoggerFactory.getLogger(App.class);
    List<Product> products = new ArrayList<>();

    void loadProducts(String classpathUri) {
        InputStream is = getClass().getResourceAsStream(classpathUri);
        if (is == null) {
            throw new RuntimeException("cannot open " + classpathUri);
        }
        products = new BufferedReader(new InputStreamReader(is))
                .lines()
                .skip(1)
                .map(Product::fromCSV)
                .collect(Collectors.toList());
        log.info("Loaded {} products", products.size());
    }

    void config() {
        port(8888);
        notFound((req, res) -> {
            res.status(405);
            return String.format("%s %s", req.requestMethod(), req.uri());
        });
        after((req, res) -> {
            res.type(JSON);
        });
    }

    void routes() {
        final Gson gson = new Gson();

        post("/products", JSON, (req, res) -> {
            Product p = gson.fromJson(req.body(), Product.class);
            if (p == null) halt(400);
            products.add(p.assignId());
            res.status(201);
            return p;
        }, gson::toJson);

        get("/products", (req, res) -> products, gson::toJson);

        get("/product/:id", (req, res) -> {
            int id = Integer.parseInt(req.params("id"));
            Optional<Product> product = find(id);
            if (!product.isPresent()) {
                halt(404);
                return null;
            }

            return product.get();
        }, gson::toJson);

        put("/product/:id", (req, res) -> {
            int id = Integer.parseInt(req.params("id"));
            Optional<Product> productOptional = find(id);
            if (!productOptional.isPresent()) {
                halt(404);
                return null;
            }

            Product product = productOptional.get();
            Product updatedProduct = gson.fromJson(req.body(), Product.class);
            if (updatedProduct.getName() != null) {
                product.setName(updatedProduct.getName());
            }
            if (updatedProduct.getPrice() > 0) {
                product.setPrice(updatedProduct.getPrice());
            }
            if (updatedProduct.getItems() > 0) {
                product.setItems(updatedProduct.getItems());
            }
            return product;

        }, gson::toJson);

        delete("/product/:id", (req, res) -> {
            int id = Integer.parseInt(req.params("id"));
            Optional<Product> product = find(id);
            if (!product.isPresent()) {
                halt(404);
                return null;
            }

            products.remove(product.get());
            res.status(204);
            return null;
        });
    }

    Optional<Product> find(long id) {
        return products.stream()
                .filter(p -> p.getId() == id)
                .findFirst();
    }

}
