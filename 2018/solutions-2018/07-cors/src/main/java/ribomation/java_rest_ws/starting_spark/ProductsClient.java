package ribomation.java_rest_ws.starting_spark;

import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class ProductsClient {
    public static void main(String[] args) throws Exception {
        ProductsClient app = new ProductsClient();
        app.run();
    }

    void run() throws Exception {
        crud();
        some(7);
    }

    void crud() throws Exception {
        ProductJSON P = new ProductJSON();
        P.setName("Banana Box");
        P.setPrice(256);
        P.setItems(10);
        P.setModified(new Date());

        Product p = create(P);
        System.out.printf("pst: %s%n", p);

        p = get(p.getId());
        System.out.printf("get: %s%n", p);

        P.setId(p.getId());
        P.setPrice(500);
        P.setItems(1);
        p = replace(P);
        System.out.printf("put: %s%n", p);

        remove(p.getId());
        System.out.printf("del: %s%n", p);
        try {
            p = get(p.getId());
            System.out.printf("get: %s%n", p);
        } catch (Exception e) {
            System.out.printf("EXPECTED: %s%n", e);
        }
    }

    void some(int n) throws Exception {
        System.out.println("--- Some Products ---");
        list().stream()
                .limit(n)
                .forEach(System.out::println);
    }


    // ----------------------------------------------
    // --- DAO API ---
    List<Product> list() throws Exception {
        HttpURLConnection con = open(resource(), "GET");
        try (InputStreamReader reader = new InputStreamReader(con.getInputStream())) {
            return new GsonBuilder()
                    .setDateFormat("yyyy-MM-dd-HH-mm-ss")
                    .create()
                    .fromJson(reader, new TypeToken<ArrayList<Product>>() {
                    }.getType());
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    Product create(ProductJSON p) throws Exception {
        HttpURLConnection con = open(resource(), "POST");
        send(p, con.getOutputStream());
        return recv(con.getInputStream());
    }

    Product get(long id) throws Exception {
        HttpURLConnection con = open(resource(id), "GET");
        return recv(con.getInputStream());
    }

    Product replace(ProductJSON p) throws Exception {
        HttpURLConnection con = open(resource(p.getId()), "PUT");
        send(p, con.getOutputStream());
        return recv(con.getInputStream());
    }

    void remove(long id) throws Exception {
        HttpURLConnection con  = open(resource(id), "DELETE");
        int               code = con.getResponseCode();
        if (code != 204) {
            throw new RuntimeException("remove failed: HTTP code=" + code);
        }
    }


    // ----------------------------------------------
    // --- HTTP API ---
    final String HOST     = "localhost";
    final int    PORT     = 9000;
    final String RESOURCE = "product";

    URL resource() throws MalformedURLException {
        return resource(HOST, PORT, RESOURCE, -1);
    }

    URL resource(long id) throws MalformedURLException {
        return resource(HOST, PORT, RESOURCE, id);
    }

    URL resource(String host, int port, String resource, long id) throws MalformedURLException {
        if (id > 0) {
            return new URL(String.format("http://%s:%d/%s/%d", host, port, resource, id));
        }
        return new URL(String.format("http://%s:%d/%ss", host, port, resource));
    }

    void send(ProductJSON p, OutputStream os) throws IOException {
        OutputStreamWriter w = new OutputStreamWriter(os, "US-ASCII");
        w.write(p.toJSON());
        w.flush();
        w.close();
    }

    Product recv(InputStream is) {
        BufferedReader in = new BufferedReader(new InputStreamReader(is));
        return in.lines()
                .map(ProductJSON::fromJSON)
                .findFirst()
                .orElse(null);
    }

    HttpURLConnection open(URL url, String op) throws IOException {
        HttpURLConnection con = (HttpURLConnection) url.openConnection();
        con.setRequestMethod(op);
        con.setDoOutput(!op.equals("GET") || !op.equals("DELETE"));
        con.setDoInput(true);
        con.setConnectTimeout(10 * 1000);
        con.setRequestProperty("Accept", "*/*");
        con.setInstanceFollowRedirects(true);
        con.connect();
        return con;
    }

}
