package ribomation.java_rest_ws.starting_spark;
import static spark.Spark.*;

public class AssetsServer {
    public static void main(String[] args) throws Exception {
        AssetsServer app = new AssetsServer();
        app.run();
    }

    void run() throws Exception {
        port(9000);
        staticFiles.location("/assets");
        get("/", (req, res) -> "Assets Server");
    }
}
