package ribomation.java_rest_ws.starting_spark;
import com.google.gson.GsonBuilder;

public class ProductJSON extends Product {
    public ProductJSON() {
    }

    public String toJSON() {
        return new GsonBuilder()
                .setPrettyPrinting()
                .setDateFormat("yyyy-MM-dd-HH-mm-ss")
                .create()
                .toJson(this);
    }

    public static Product fromJSON(String json) {
        return new GsonBuilder()
                .setDateFormat("yyyy-MM-dd-HH-mm-ss")
                .create()
                .fromJson(json, Product.class);
    }
}
