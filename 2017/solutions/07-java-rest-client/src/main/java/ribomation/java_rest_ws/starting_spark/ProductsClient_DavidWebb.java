package ribomation.java_rest_ws.starting_spark;
import com.goebl.david.Webb;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class ProductsClient_DavidWebb extends ProductsClient {
    public static void main(String[] args) throws Exception {
        ProductsClient_DavidWebb app = new ProductsClient_DavidWebb();
        app.run();
    }

    @Override
    List<Product> list() throws Exception {
        System.out.println("** DavidWebb: list()");
        JSONArray json = Webb.create()
                .get(resource().toString())
                .ensureSuccess()
                .asJsonArray()
                .getBody();

        List<Product> objs = new ArrayList<>();
        for (int k = 0; k < json.length(); ++k) {
            objs.add(ProductJSON.fromJSON(json.getJSONObject(k).toString()));
        }
        return objs;
    }

    @Override
    Product create(ProductJSON p) throws Exception {
        System.out.println("** DavidWebb: create()");
        JSONObject json = Webb.create()
                .post(resource().toString())
                .body(p.toJSON())
                .ensureSuccess()
                .asJsonObject()
                .getBody();
        return ProductJSON.fromJSON(json.toString());
    }

    @Override
    Product get(long id) throws Exception {
        System.out.println("** DavidWebb: get()");
        JSONObject json = Webb.create()
                .get(resource(id).toString())
                .ensureSuccess()
                .asJsonObject()
                .getBody();
        return ProductJSON.fromJSON(json.toString());
    }

    @Override
    Product replace(ProductJSON p) throws Exception {
        System.out.println("** DavidWebb: replace()");
        JSONObject json = Webb.create()
                .put(resource(p.getId()).toString())
                .body(p.toJSON())
                .ensureSuccess()
                .asJsonObject()
                .getBody();
        return ProductJSON.fromJSON(json.toString());
    }

    @Override
    void remove(long id) throws Exception {
        System.out.println("** DavidWebb: remove()");
        Webb.create()
                .delete(resource(id).toString())
                .ensureSuccess()
                .asVoid();
    }

}
