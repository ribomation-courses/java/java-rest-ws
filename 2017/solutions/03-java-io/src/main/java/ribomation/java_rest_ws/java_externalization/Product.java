package ribomation.java_rest_ws.java_externalization;


import ribomation.java_rest_ws.ProductType;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;
import java.util.Objects;

public class Product implements Externalizable, ProductType {
    private String name;
    private float  price;
    private int    items;

    public Product() {
    }

    public Product(String name, float price, int items) {
        this.name = name;
        this.price = price;
        this.items = items;
    }

    @Override
    public void writeExternal(ObjectOutput out) throws IOException {
        out.writeUTF(name);
        out.writeFloat(price);
        out.writeInt(items);
    }

    @Override
    public void readExternal(ObjectInput in) throws IOException, ClassNotFoundException {
        name = in.readUTF();
        price = in.readFloat();
        items = in.readInt();
    }

    @Override
    public String toString() {
        return "Product{" +
                "name='" + name + '\'' +
                ", price=" + price +
                ", items=" + items +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Product product = (Product) o;
        return Float.compare(product.price, price) == 0 &&
                items == product.items &&
                Objects.equals(name, product.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, price, items);
    }

    public String getName() {
        return name;
    }

    public float getPrice() {
        return price;
    }

    public int getItems() {
        return items;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setPrice(float price) {
        this.price = price;
    }

    public void setItems(int items) {
        this.items = items;
    }
}
