package ribomation.java_rest_ws.java_serialization;

import ribomation.java_rest_ws.ProductType;
import java.io.Serializable;
import java.util.Objects;

public class Product implements Serializable, ProductType {
    private String name;
    private float  price;
    private int    items;

    public Product() {
    }

    public Product(String name, float price, int items) {
        this.name = name;
        this.price = price;
        this.items = items;
    }

    @Override
    public String toString() {
        return "Product{" +
                "name='" + name + '\'' +
                ", price=" + price +
                ", items=" + items +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Product product = (Product) o;
        return Float.compare(product.price, price) == 0 &&
                items == product.items &&
                Objects.equals(name, product.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, price, items);
    }

    public String getName() {
        return name;
    }

    public float getPrice() {
        return price;
    }

    public int getItems() {
        return items;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setPrice(float price) {
        this.price = price;
    }

    public void setItems(int items) {
        this.items = items;
    }
}
