package ribomation.java_rest_ws;

public interface ProductType {
    void setName(String name);

    void setPrice(float price);

    void setItems(int items);
}
