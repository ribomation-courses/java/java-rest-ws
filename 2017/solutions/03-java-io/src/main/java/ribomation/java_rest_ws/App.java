package ribomation.java_rest_ws;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;

public class App {
    public static void main(String[] args) throws Exception {
        App app = new App();
        app.run(ribomation.java_rest_ws.java_serialization.Product.class);
        app.run(ribomation.java_rest_ws.java_externalization.Product.class);
    }

    void run(Class<? extends ProductType> cls) throws Exception {
        System.out.printf("--- %s ---%n", cls.getPackage().getName().split("\\.")[2]);
        ProductType product = mk(cls);
        System.out.printf("product: %s%n", product);
        long size = pack((Serializable) product);
        System.out.printf("Payload-Size: %d%n", size);
    }

    ProductType mk(Class<? extends ProductType> cls) throws Exception {
        ProductType obj = cls.getConstructor().newInstance();
        obj.setName("Banana Box");
        obj.setPrice(512);
        obj.setItems(42);
        return obj;
    }

    long pack(Serializable obj) throws Exception {
        ByteArrayOutputStream obuf = new ByteArrayOutputStream(1024);
        ObjectOutputStream    oos  = new ObjectOutputStream(obuf);
        oos.writeObject(obj);
        oos.close();

        byte[] payload     = obuf.toByteArray();
        long   payloadSize = payload.length;

        ByteArrayInputStream ibuf = new ByteArrayInputStream(payload);
        ObjectInputStream    ois  = new ObjectInputStream(ibuf);
        Object               obj2 = ois.readObject();

        if (!obj.equals(obj2)) {
            System.out.println("*** Failure ***");
            System.out.printf("obj-2: %s%n", obj2);
        }
        return payloadSize;
    }

}
