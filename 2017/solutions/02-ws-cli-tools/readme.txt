
Info: https://jsonplaceholder.typicode.com/

REST URI: https://jsonplaceholder.typicode.com/<resource>

Resources:
/posts	100 items
/comments	500 items
/albums	100 items
/photos	5000 items
/todos	200 items
/users	10 items

Samples using HTTPie:

http https://jsonplaceholder.typicode.com/posts/2

http https://jsonplaceholder.typicode.com/users

http POST https://jsonplaceholder.typicode.com/todos title=tjolla+hopp

http PUT https://jsonplaceholder.typicode.com/comments/3 name=hipp body=jabba+dabba+dooo

http DELETE https://jsonplaceholder.typicode.com/comments/3

