#!/usr/bin/env bash
set -e
set -u

uriAll=":9000/products"
uriOne=":9000/product"

echo "*** NEW product"
POST=`http POST $uriAll name="Java REST Book" price=512 items=10`
echo $POST
id=`echo $POST | jq '.id'`
echo "product ID=$id"

set -x
http GET "$uriOne/$id"

http PUT "$uriOne/$id" price=256 items=5
http GET "$uriOne/$id"

http DELETE "$uriOne/$id"
http GET "$uriOne/$id"
