package ribomation.java_rest_ws.starting_spark;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Objects;

public class Product {
    private long   id;
    private String name;
    private float  price;
    private int    items;
    private Date   modified;

    public Product() {
    }

    public static Product fromCSV(String csv) {
        return fromCSV(csv, ";");
    }

    public static Product fromCSV(String csv, String delim) {
        try {
            String[] fields = csv.split(delim);
            return new Product(fields[0], fields[1], fields[2], fields[3]);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public Product(String name, String price, String items, String modified) throws Exception {
        //name,price,items,modified
        //"Basil - Primerba, Paste",619.19,12,2017-08-29 16:24:45
        this(name.replaceAll("\"", ""),
                Float.parseFloat(price),
                Integer.parseInt(items),
                new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(modified));
    }

    public Product(String name, float price, int items, Date modified) {
        this.name = name;
        this.price = price;
        this.items = items;
        this.modified = modified;
    }

    Product update(Product p) {
        if (p.name != null && p.name.trim().length() > 0) {
            this.name = p.name;
        }
        if (p.price > 0) {
            this.price = p.price;
        }
        if (p.items > -1) {
            this.items = p.items;
        }
        if (p.modified != null) {
            this.modified = p.modified;
        }
        return this;
    }

    @Override
    public String toString() {
        return "Product{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", price=" + price +
                ", items=" + items +
                ", modified=" + modified +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Product product = (Product) o;
        return id == product.id;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

    public long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public float getPrice() {
        return price;
    }

    public int getItems() {
        return items;
    }

    public Date getModified() {
        return modified;
    }


    public void setId(long id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setPrice(float price) {
        this.price = price;
    }

    public void setItems(int items) {
        this.items = items;
    }

    public void setModified(Date modified) {
        this.modified = modified;
    }
}
