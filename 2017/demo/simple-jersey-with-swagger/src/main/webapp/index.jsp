<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Jersey-Swagger</title>
</head>
<body>
    <h1>Jersey-Swagger Demo</h1>
    <ul>
        <li>
            <a href="app/books">REST-WS App</a>
        </li>
        <li>
            <a href="docs/">Swagger Docs</a>
        </li>
        <li>
            <a href="app/swagger.json">Swagger JSON</a>
        </li>
        <li>
            <a href="app/swagger.yaml">Swagger YaML</a>
        </li>
    </ul>
</body>
</html>
