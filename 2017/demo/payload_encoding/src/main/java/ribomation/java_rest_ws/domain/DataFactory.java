package ribomation.java_rest_ws.domain;

import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class DataFactory {
    private Random r = new Random();

    public List<Person> mkPersons() {
        return mkPersons(1 + r.nextInt(10));
    }

    public List<Person> mkPersons(int n) {
        return IntStream.range(0, n)
                .mapToObj(x -> mkPerson())
                .collect(Collectors.toList())
                ;
    }

    public Person mkPerson() {
        return new Person()
                .name(mkName())
                .age(20 + r.nextInt(50))
                .female(r.nextBoolean())
                .lastUpdate(mkDate())
                ;
    }

    private Date mkDate() {
        int  delta = r.nextInt(20 * 24 * 3600 * 1000);
        long ts    = System.currentTimeMillis() - delta;
        return new Date(ts);
    }

    private String mkName() {
        return names.get(r.nextInt(names.size()));
    }

    private static List<String> names = Arrays.asList(
            "Anna Conda", "Justin Time", "Per Silja", "Sham Poo",
            "Al Dente", "Bill Board", "Cara Wan", "Chris P. Bacon",
            "Di Liver", "Doug Hole", "Frank Enstein", "Gene Pool"
    );

    public static void main(String[] args) {
        new DataFactory()
                .mkPersons()
                .forEach(System.out::println);
        ;
    }
}
