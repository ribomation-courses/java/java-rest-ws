package ribomation.java_rest_ws.codecs;

import org.supercsv.cellprocessor.FmtBool;
import org.supercsv.cellprocessor.FmtDate;
import org.supercsv.cellprocessor.FmtNumber;
import org.supercsv.cellprocessor.ParseBool;
import org.supercsv.cellprocessor.ParseDate;
import org.supercsv.cellprocessor.ParseInt;
import org.supercsv.cellprocessor.constraint.NotNull;
import org.supercsv.cellprocessor.ift.CellProcessor;
import org.supercsv.io.CsvBeanReader;
import org.supercsv.io.CsvBeanWriter;
import org.supercsv.prefs.CsvPreference;
import ribomation.java_rest_ws.Codec;
import ribomation.java_rest_ws.domain.Person;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.List;


public class CSV implements Codec {
    private static final CsvPreference   csvPreference;
    private static final String[]        csvHeader;
    private static final CellProcessor[] outFmts;
    private static final CellProcessor[] inFmts;

    static {
        csvPreference = CsvPreference.EXCEL_NORTH_EUROPE_PREFERENCE;
        csvHeader = new String[]{
                "name", "age", "female", "lastUpdate"
        };
        outFmts = new CellProcessor[]{
                new NotNull(),
                new NotNull(),
                new NotNull(new FmtBool("F", "M")),
                new NotNull(new FmtDate("yyyy-MM-dd HH:mm:ss"))
        };
        inFmts = new CellProcessor[]{
                new NotNull(),
                new NotNull(new ParseInt()),
                new NotNull(new ParseBool("F", "M")),
                new NotNull(new ParseDate("yyyy-MM-dd HH:mm:ss"))
        };
    }

    @Override
    public void encode(List<Person> objs, OutputStream out) {
        try (OutputStreamWriter writer = new OutputStreamWriter(out)) {
            CsvBeanWriter beanWriter = new CsvBeanWriter(writer, csvPreference);
            beanWriter.writeHeader(csvHeader);
            for (Person p : objs) {
                beanWriter.write(p, csvHeader, outFmts);
            }
            beanWriter.close();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public List<Person> decode(InputStream in) {
        try (InputStreamReader reader = new InputStreamReader(in)) {
            CsvBeanReader  beanReader = new CsvBeanReader(reader, csvPreference);
            List<Person>   restored   = new ArrayList<>();
            final String[] header     = beanReader.getHeader(true);
            for (Person p; (p = beanReader.read(Person.class, header, inFmts)) != null; ) {
                restored.add(p);
            }
            beanReader.close();
            return restored;
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
