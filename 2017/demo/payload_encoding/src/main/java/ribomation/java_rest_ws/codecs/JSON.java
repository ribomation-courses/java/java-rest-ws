package ribomation.java_rest_ws.codecs;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import ribomation.java_rest_ws.Codec;
import ribomation.java_rest_ws.domain.Person;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;


public class JSON implements Codec {
    private static final Type TYPE =
            new TypeToken<ArrayList<Person>>() {}.getType();

    @Override
    public void encode(List<Person> objs, OutputStream out) {
        try (OutputStreamWriter writer = new OutputStreamWriter(out)) {
            new Gson().toJson(objs, TYPE, writer);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public List<Person> decode(InputStream in) {
        try (InputStreamReader reader = new InputStreamReader(in)) {
            return new Gson().fromJson(reader, TYPE);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}


