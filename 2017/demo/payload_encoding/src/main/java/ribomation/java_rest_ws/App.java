package ribomation.java_rest_ws;

import org.apache.commons.lang.text.StrBuilder;
import ribomation.java_rest_ws.codecs.CSV;
import ribomation.java_rest_ws.codecs.FWV;
import ribomation.java_rest_ws.codecs.Hessian;
import ribomation.java_rest_ws.codecs.JSON;
import ribomation.java_rest_ws.codecs.XML;
import ribomation.java_rest_ws.domain.DataFactory;
import ribomation.java_rest_ws.domain.Person;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public class App {
    public static void main(String[] args) {
        new App().run();
    }

    void run() {
        List<Person> target = new DataFactory().mkPersons(5);
        target.forEach(System.out::println);
        run(target, new Codec[]{
                new XML(),
                new JSON(),
                new CSV(),
                new FWV(),
                new Hessian()
        });
    }

    void run(List<Person> target, Codec[] codecs) {
        Map<Codec, Integer> results = new LinkedHashMap<>();
        for (Codec codec : codecs) {
            int size = run(target, codec, codec.isText());
            results.put(codec, size);
        }

        System.out.printf("--- Payload Size ---%n");
        results.forEach((codec, size) ->
                System.out.printf("%-7s: %6d bytes%n",
                        codec.getClass().getSimpleName(), size));
    }

    int run(List<Person> target, Codec codec, boolean text) {
        System.out.printf("--- %s ---%n", codec.getClass().getSimpleName());
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        codec.encode(target, out);
        byte[] payload = out.toByteArray();

        if (text) {
            System.out.println(new String(payload));
        } else {
            System.out.println(toHEX(payload));
            System.out.println();
        }

        ByteArrayInputStream in       = new ByteArrayInputStream(payload);
        List<Person>         restored = codec.decode(in);
        if (notEquals(target, restored)) {
            System.out.println("*** Restored list =/= target");
            restored.forEach(System.out::println);
        }

        return payload.length;
    }

    boolean notEquals(List<Person> lst1, List<Person> lst2) {
        if (lst1 == null || lst2 == null) {
            System.out.printf("*** null lists: %n");
            return true;
        }

        if (lst1.size() != lst2.size()) {
            System.out.printf("*** size differ: %d =/= %d%n", lst1.size(), lst2.size());
            return true;
        }

        for (int k = 0; k < lst1.size(); ++k) {
            Person p1 = lst1.get(k);
            Person p2 = lst2.get(k);
            if (!p1.equals(p2)) {
                System.out.printf("*** %s%n=/= %s%n", p1, p2);
                return true;
            }
        }

        return false;
    }

    String toHEX(byte[] data) {
        return new sun.misc.HexDumpEncoder().encode(data);
//        StrBuilder buf = new StrBuilder(10_000);
//        for (byte oneByte : data) {
//            String value = Integer.toHexString(Byte.toUnsignedInt(oneByte) % 255);
//            if (value.length() == 1) value = '0' + value;
//            buf.append(value).append(" ");
//        }
//        return buf.toString().toUpperCase();
    }
}
