package ribomation.java_rest_ws.jwt;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.MalformedJwtException;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.SignatureException;
import io.jsonwebtoken.UnsupportedJwtException;
import io.jsonwebtoken.impl.crypto.MacProvider;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import spark.ModelAndView;
import spark.Response;
import spark.template.handlebars.HandlebarsTemplateEngine;

import java.security.Key;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import static spark.Spark.*;

public class AuthServer {
    final String AUDIENCE     = "http://localhost:8000/";
    final String ISSUER       = "ribomation";
    final int    VALID_PERIOD = 1 * 60 * 1000;
    final Logger LOG;
    final Key    secretKey;

    public static void main(String[] args) {
        int port = args.length == 0 ? 8000 : Integer.parseInt(args[0]);
        new AuthServer().run(port);
    }

    public AuthServer() {
        System.setProperty("org.slf4j.simpleLogger.showShortLogName", "true");
        System.setProperty("org.slf4j.simpleLogger.showThreadName", "false");
        this.LOG = LoggerFactory.getLogger(AuthServer.class);
        this.secretKey = MacProvider.generateKey();
    }

    void run(int port) {
        configure(port);
        routes();
        LOG.info("Started http://localhost:{}/%n", port);
    }

    void configure(int port) {
        port(port);

        exception(Exception.class, (err, req, res) -> {
            err.printStackTrace();
            res.status(500);
            res.body(err.toString());
        });

        notFound((req, res) -> {
            res.status(404);
            return null;
        });

        before((req, res) -> {
            LOG.info("[log] {} {}", req.requestMethod(), req.uri());
        });

        before("/protected/*", (req, res) -> {
            LOG.info("[prot] uri={}", req.uri());

            String jwt = req.queryParams("jwt");
            LOG.info("[prot] jwt={}", jwt);
            if (jwt == null || !isValid(jwt)) {
                LOG.info("[prot] DENIED");
                res.redirect("/auth/login?msg=Must+login");
            }
        });
    }

    void routes() {
        get("/", (req, res) -> {
            res.redirect("/auth/login", 301);
            return null;
        });

        get("/auth/login", (req, res) -> render("login", res));

        post("/auth/login", (req, res) -> {
            String uid = req.queryParams("uid");
            String pwd = req.queryParams("pwd");
            LOG.info("[login] uid={}, pwd={}", uid, pwd);

            if (pwd.equals("hepp")) {
                String jwt = createJWT(uid);
                LOG.info("[login] OK jwt={}", jwt);
                res.redirect("/protected/dashboard?jwt=" + jwt);
            } else {
                LOG.info("[login] DENIED");
                res.redirect("/auth/login?msg=Invalid+password");
            }
            return null;
        });

        get("/protected/:name", (req, res) -> {
            return render(req.params("name"), res);
        });
    }

    String render(String view, Response r) {
        return render(new HashMap<>(), view, r);
    }

    String render(Map<String, Object> model, String view, Response r) {
        r.type("text/html");
        return new HandlebarsTemplateEngine()
                .render(new ModelAndView(model, view + ".html"));
    }

    String createJWT(String uid) {
        return Jwts.builder()
                .setAudience(AUDIENCE)
                .setIssuer(ISSUER)
                .setSubject(uid)
                .setExpiration(
                        new Date(System.currentTimeMillis()
                                + VALID_PERIOD))
                .signWith(SignatureAlgorithm.HS512, secretKey)
                .compact();
    }

    boolean isValid(String jwt) {
        try {
            Claims claims = Jwts.parser()
                    .setSigningKey(secretKey)
                    .parseClaimsJws(jwt)
                    .getBody();
            LOG.info("[claims] {}", claims);

            if (!claims.getAudience().equals(AUDIENCE)) return false;
            if (!claims.getIssuer().equals(ISSUER)) return false;
            if (claims.getExpiration().before(new Date())) return false;

            return true;
        } catch (ExpiredJwtException e) {
            LOG.warn("[claims] expired jwt: {}", e.getMessage());
        } catch (UnsupportedJwtException e) {
            LOG.warn("[claims] unsupported jwt: {}", e.getMessage());
        } catch (MalformedJwtException e) {
            LOG.warn("[claims] malformed jwt: {}", e.getMessage());
        } catch (SignatureException e) {
            LOG.warn("[claims] invalid signature: {}", e.getMessage());
        } catch (IllegalArgumentException e) {
            LOG.warn("[claims] unexpected: {}", e.getMessage());
        }
        return false;
    }
}
