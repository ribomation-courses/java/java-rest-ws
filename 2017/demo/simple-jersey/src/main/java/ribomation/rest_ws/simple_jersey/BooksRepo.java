package ribomation.rest_ws.simple_jersey;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.concurrent.atomic.AtomicLong;

public class BooksRepo {
    public static final BooksRepo       instance = new BooksRepo();
    private final       Map<Long, Book> repo     = new TreeMap<>();
    private final       AtomicLong      nextId   = new AtomicLong(1);

    public List<Book> list() {
        return new ArrayList<>(repo.values());
    }
    public Book       get(long id) {
        return repo.get(id);
    }
    public boolean    delete(long id) {
        return repo.remove(id) != null;
    }

    public Book create(Book book) {
        book.setId(nextId.getAndIncrement());
        repo.put(book.getId(), book);
        return book;
    }

    public Book update(long id, Book delta) {
        Book book = get(id);
        if (book == null) return null;
        if (delta.getTitle() != null) book.setTitle(delta.getTitle());
        if (delta.getAuthor() != null) book.setAuthor(delta.getAuthor());
        if (delta.getPrice() > 0) book.setPrice(delta.getPrice());
        return book;
    }

    public void populate() {
        create(new Book("Java Programming", "James Gosling", 512));
        create(new Book("Groovy Up&Running", "James Strachan", 256));
        create(new Book("C++ Basics", "Bjarne Stroustrup", 1024));
        create(new Book("The C Programming Language", "Dennis Ritchie", 128));
    }

    /**
     * used only for unit tests
     */
    /*package*/  void clear() {
        repo.clear();
        nextId.set(1);
    }
}
