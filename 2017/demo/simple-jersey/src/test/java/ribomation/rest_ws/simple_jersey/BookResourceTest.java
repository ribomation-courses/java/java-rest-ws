package ribomation.rest_ws.simple_jersey;

import org.glassfish.jersey.client.ClientConfig;
import org.glassfish.jersey.moxy.json.MoxyJsonFeature;
import org.glassfish.jersey.server.ResourceConfig;
import org.glassfish.jersey.test.JerseyTest;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import javax.ws.rs.client.Entity;
import javax.ws.rs.core.Application;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;

import static org.junit.Assert.*;


public class BookResourceTest extends JerseyTest {
    @Override
    protected Application configure() {
        return new ResourceConfig(BookResource.class);
    }

    @Override
    protected void configureClient(ClientConfig config) {
        config.register(MoxyJsonFeature.class);
    }

    @Before
    public void init() {
        BooksRepo.instance.populate();
    }

    @After
    public void finit() {
        BooksRepo.instance.clear();
    }

    @Test
    public void get_one_book() {
        Book book = target("books/1")
                .request(MediaType.APPLICATION_JSON_TYPE)
                .get(Book.class);
        assertNotNull(book);
        assertEquals("Java Programming", book.getTitle());
    }

    @Test
    public void get_all_books() {
        List<Book> books = target("books")
                .request(MediaType.APPLICATION_JSON_TYPE)
                .get(new GenericType<List<Book>>() {
                });
        assertNotNull(books);
        assertFalse(books.isEmpty());
        assertEquals(4, books.size());

        Book book = books.get(books.size() - 1);
        assertNotNull(book);
        assertEquals("The C Programming Language", book.getTitle());
    }

    @Test
    public void create_newBook() {
        List<Book> books = target("books")
                .request(MediaType.APPLICATION_JSON_TYPE)
                .get(new GenericType<List<Book>>() {
                });

        Book book = new Book("Jersey Primer", "Justin Time", 999);

        Book createdBook = target("books")
                .request(MediaType.APPLICATION_JSON_TYPE)
                .post(Entity.json(book))
                .readEntity(Book.class);

        assertNotNull(createdBook);
        assertEquals(book.getTitle(), createdBook.getTitle());
        assertEquals(book.getPrice(), createdBook.getPrice());
        assertEquals(books.get(books.size() - 1).getId() + 1, createdBook.getId());
    }

    @Test
    public void update_book() {
        Book oldBook = target("books/1")
                .request(MediaType.APPLICATION_JSON_TYPE)
                .get(Book.class);
        assertNotNull(oldBook);

        Book book = new Book();
        book.setTitle("Jersey Kick-Start");

        Book updatedBook = target("books/1")
                .request(MediaType.APPLICATION_JSON_TYPE)
                .put(Entity.json(book))
                .readEntity(Book.class);

        assertNotNull(updatedBook);
        assertEquals(book.getTitle(), updatedBook.getTitle());
        assertEquals(oldBook.getPrice(), updatedBook.getPrice());
        assertEquals(oldBook.getAuthor(), updatedBook.getAuthor());
        assertEquals(oldBook.getId(), updatedBook.getId());
    }

    @Test
    public void delete_book() {
        List<Book> books = target("books")
                .request(MediaType.APPLICATION_JSON_TYPE)
                .get(new GenericType<List<Book>>() {
                });
        assertNotNull(books);
        final int N = books.size();

        Response res = target("books/1")
                .request(MediaType.APPLICATION_JSON_TYPE)
                .delete();
        assertEquals(204, res.getStatus());

        books = target("books")
                .request(MediaType.APPLICATION_JSON_TYPE)
                .get(new GenericType<List<Book>>() {
                });
        assertNotNull(books);
        assertEquals(N - 1, books.size());
    }
}
