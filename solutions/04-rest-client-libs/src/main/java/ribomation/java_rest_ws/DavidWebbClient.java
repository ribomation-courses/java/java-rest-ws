package ribomation.java_rest_ws;

import com.goebl.david.Webb;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class DavidWebbClient {
    public static void main(String[] args) throws Exception {
        DavidWebbClient app = new DavidWebbClient();
        app.run("http://localhost:8000/products", 100);
    }

    void run(String baseUrl, int id) throws IOException, JSONException {
        var obj = get(baseUrl, id);
        System.out.printf("obj: %s%n", obj);

        obj = update(baseUrl, id, "Tjolla Hopp", 512, 42);
        System.out.printf("obj: %s%n", obj);

        System.out.printf("delete ID=%d --> %b%n", id, delete(baseUrl, id));
    }

    String get(String baseUrl, int id) throws IOException, JSONException {
        return Webb.create()
                .get(baseUrl + "/" + id)
                .ensureSuccess()
                .asJsonObject()
                .getBody()
                .toString(2)
                ;
    }

    String update(String baseUrl, int id, String name, float price, int count) throws IOException, JSONException {
        var dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm");
        return Webb.create()
                .put(baseUrl + "/" + id)
                .body(new JSONObject()
                        .put("name", name)
                        .put("price", price)
                        .put("itemsInStock", count)
                        .put("modificationDate", dateFormat.format(new Date()))
                )
                .ensureSuccess()
                .asJsonObject()
                .getBody()
                .toString(2)
                ;
    }

    boolean delete(String baseUrl, int id) throws IOException {
        return Webb.create()
                .delete(baseUrl + "/" + id)
                .ensureSuccess()
                .asVoid()
                .getStatusCode() == 204
                ;
    }

}
