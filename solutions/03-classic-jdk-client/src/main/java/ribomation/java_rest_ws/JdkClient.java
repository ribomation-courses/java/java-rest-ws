package ribomation.java_rest_ws;

import com.google.gson.GsonBuilder;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.io.StringWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.text.SimpleDateFormat;
import java.util.Date;

public class JdkClient {
    public static void main(String[] args) throws Exception {
        JdkClient app = new JdkClient();
        app.run("http://localhost:8000/products", 100);
    }

    void run(String baseUrl, int id) throws IOException {
        var obj = get(baseUrl, id);
        System.out.printf("obj: %s%n", obj);

        obj = update(baseUrl, id, "Tjolla Hopp", 512, 42);
        System.out.printf("obj: %s%n", obj);

        System.out.printf("delete ID=%d --> %b%n", id, delete(baseUrl, id));
    }

    String get(String baseUrl, int id) throws IOException {
        var url = new URL(baseUrl + "/" + id);
        var con = (HttpURLConnection) url.openConnection();

        con.setRequestMethod("GET");
        con.setDoOutput(false);
        con.setDoInput(true);
        con.setInstanceFollowRedirects(true);
        con.connect();

        var buf = new ByteArrayOutputStream();
        con.getInputStream().transferTo(buf);
        con.disconnect();

        return buf.toString(StandardCharsets.US_ASCII);
    }

    String update(String baseUrl, int id, String name, float price, int count) throws IOException {
        var url = new URL(baseUrl + "/" + id);
        var con = (HttpURLConnection) url.openConnection();

        con.setRequestMethod("PUT");
        con.setDoOutput(true);
        con.setDoInput(true);
        con.setInstanceFollowRedirects(true);
        con.connect();

        var dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm");
        var gson = new GsonBuilder()
                .setPrettyPrinting()
                .setDateFormat(dateFormat.toPattern())
                .create();

        var outbuf = new StringWriter();
        gson.newJsonWriter(outbuf)
                .beginObject()
                    .name("name").value(name)
                    .name("price").value(price)
                    .name("itemsInStock").value(count)
                    .name("modificationDate").value(dateFormat.format(new Date()))
                .endObject()
                .close();

        var out  = new PrintStream(con.getOutputStream());
        try (out) {
            out.println(outbuf.toString());
        }

        var inbuf = new ByteArrayOutputStream();
        con.getInputStream().transferTo(inbuf);
        con.disconnect();

        return inbuf.toString(StandardCharsets.US_ASCII);
    }

    boolean delete(String baseUrl, int id) throws IOException {
        var url = new URL(baseUrl + "/" + id);
        var con = (HttpURLConnection) url.openConnection();

        con.setRequestMethod("DELETE");
        con.setDoOutput(false);
        con.setDoInput(true);
        con.setInstanceFollowRedirects(true);
        con.connect();

        var code = con.getResponseCode();
        return (code == 204);
    }

}
