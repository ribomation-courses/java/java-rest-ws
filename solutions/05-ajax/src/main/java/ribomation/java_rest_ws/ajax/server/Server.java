package ribomation.java_rest_ws.ajax.server;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import ribomation.java_rest_ws.ajax.Product;
import ribomation.java_rest_ws.ajax.ProductRepo;

import static spark.Spark.*;

public class Server {
    public static void main(String[] args) {
        Server app = new Server();
        app.setup();
        app.init();
        app.routes();
    }

    private final String      JSON = "application/json";
    private       ProductRepo repo;
    private       Gson        gson;

    private void setup() {
        repo = new ProductRepo();
        //repo.getProducts().forEach(System.out::println);

        gson = new GsonBuilder()
                .setPrettyPrinting()
                .setDateFormat("yyyy-MM-dd")
                .create();
    }

    private void init() {
        port(8000);
        notFound((req, res) -> "Oooops: " + req.url());
        staticFiles.location("/assets");
    }

    private void routes() {
        path("/products", () -> {
            get("", (req, res) -> repo.getProducts(), gson::toJson);

            get("/:id", (req, res) -> {
                var id = Integer.parseInt(req.params("id"));
                var p  = repo.getProductById(id);
                if (p.isEmpty()) {
                    halt(404);
                }

                return p.get();
            }, gson::toJson);

            delete("/:id", (req, res) -> {
                var id = Integer.parseInt(req.params("id"));
                var p  = repo.getProductById(id);
                if (p.isEmpty()) {
                    halt(404);
                }

                repo.remove(id);
                res.status(204);
                return null;
            }, gson::toJson);

            put("/:id", JSON, (req, res) -> {
                var id = Integer.parseInt(req.params("id"));
                var p  = repo.getProductById(id);
                if (p.isEmpty()) {
                    halt(404);
                }

                var P = gson.fromJson(req.body(), Product.class);
                if (P == null) {
                    halt(400);
                }

                return p.get().update(P);
            }, gson::toJson);

            post("", JSON, (req, res) -> {
                var p = gson.fromJson(req.body(), Product.class);
                if (p == null) {
                    halt(400);
                }
                res.status(201);
                return repo.insert(p);
            }, gson::toJson);
        });

    }

}
