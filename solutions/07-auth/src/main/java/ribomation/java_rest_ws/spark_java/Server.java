package ribomation.java_rest_ws.spark_java;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.security.Keys;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import spark.Request;

import java.security.Key;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.time.temporal.TemporalUnit;
import java.util.Date;

import static spark.Spark.*;

public class Server {
    public static void main(String[] args) {
        Server app = new Server();
        app.setup();
        app.init();
        app.auth();
        app.run();
    }

    private final String      JSON     = "application/json";
    private final String      AUDIENCE = "http://localhost:8000/";
    private final String      ISSUER   = "ribomation";
    private       Logger      LOG;
    private       ProductRepo repo;
    private       Gson        gson;
    private       Key         secretKey;

    void setup() {
        LOG = LoggerFactory.getLogger(getClass());

        repo = new ProductRepo();
        //repo.getProducts().forEach(System.out::println);

        gson = new GsonBuilder()
                .setPrettyPrinting()
                .setDateFormat("yyyy-MM-dd HH:mm:ss")
                .create();

        secretKey = Keys.secretKeyFor(SignatureAlgorithm.HS256);
    }

    void init() {
        port(8000);
        notFound((req, res) -> "Oooops: " + req.url());
        exception(NullPointerException.class, (err,req,res) -> {
            LOG.warn("ERROR: {}", err.toString());
            halt(400);
        });
        before((req, res) -> {
            LOG.info("REQ: {} {}", req.requestMethod(), req.uri());
        });
        after((req, res) -> {
            res.type(JSON);
        });
    }

    void auth() {
        before("/products/*", (req, res) -> {
            if (isAuthRequired(req)) {
                var jwt = req.headers("X-Auth-Token");
                if (jwt == null || !isValid(jwt)) {
                    LOG.warn("DENIED");
                    halt(401);
                }
            }
        });

        post("/auth/login", (req, res) -> {
            var credentials = gson.fromJson(req.body(), Credentials.class);
            LOG.info("CRED: '{}/{}'", credentials.getUsername(), credentials.getPassword());

            if (!isValid(credentials)) {
                LOG.warn("DENIED");
                halt(401);
            }

            var jwt = createJWT(credentials.getUsername());
            LOG.info("JWT: {}", jwt);

            res.status(201);
            return new Token(jwt, credentials.getUsername());
        }, gson::toJson);
    }

    void run() {
        path("/products", () -> {
            get("", (req, res) -> repo.getProducts(), gson::toJson);

            get("/:id", (req, res) -> {
                var id = Integer.parseInt(req.params("id"));
                var p  = repo.getProductById(id);
                if (p.isEmpty()) {
                    halt(404);
                }

                return p.get();
            }, gson::toJson);

            delete("/:id", (req, res) -> {
                var id = Integer.parseInt(req.params("id"));
                var p  = repo.getProductById(id);
                if (p.isEmpty()) {
                    halt(404);
                }

                repo.remove(id);
                res.status(204);
                return null;
            }, gson::toJson);

            put("/:id", JSON, (req, res) -> {
                var id = Integer.parseInt(req.params("id"));
                var p  = repo.getProductById(id);
                if (p.isEmpty()) {
                    halt(404);
                }

                var P = gson.fromJson(req.body(), Product.class);
                if (P == null) {
                    halt(400);
                }

                return p.get().update(P);
            }, gson::toJson);

            post("", JSON, (req, res) -> {
                var p = gson.fromJson(req.body(), Product.class);
                if (p == null) {
                    halt(400);
                }
                res.status(201);
                return repo.insert(p);
            }, gson::toJson);

        });

    }

    boolean isValid(Credentials c) {
        if (c == null) return false;
        if (c.getUsername() == null) return false;
        if (c.getPassword() == null) return false;
        if (c.getUsername().trim().length() == 0) return false;
        if (c.getPassword().trim().length() == 0) return false;

        if (!c.getUsername().equals("admin")) return false;
        if (!c.getPassword().equals("admin")) return false;

        return true;
    }

    boolean isAuthRequired(Request r) {
        var m = r.requestMethod();
        if (m.equals("POST")) return true;
        if (m.equals("PUT")) return true;
        if (m.equals("DELETE")) return true;
        return false;
    }

    String createJWT(String uid) {
        return Jwts.builder()
                .setAudience(AUDIENCE)
                .setIssuer(ISSUER)
                .setSubject(uid)
                .setNotBefore(Date.from(Instant.now()))
                .setExpiration(Date.from(Instant.now().plus(5, ChronoUnit.MINUTES)))
                .signWith(secretKey)
                .compact();
    }

    boolean isValid(String jwt) {
        try {
            Claims claims = Jwts.parser()
                    .setSigningKey(secretKey)
                    .parseClaimsJws(jwt)
                    .getBody();
            LOG.info("[claims] {}", claims);

            if (!claims.getAudience().equals(AUDIENCE)) return false;
            if (!claims.getIssuer().equals(ISSUER)) return false;

            return true;
        } catch (ExpiredJwtException e) {
            LOG.warn("[claims] expired jwt: {}", e.getMessage());
        } catch (Exception e) {
            LOG.warn("[claims] unexpected: {}", e.getMessage());
        }
        return false;
    }

}
