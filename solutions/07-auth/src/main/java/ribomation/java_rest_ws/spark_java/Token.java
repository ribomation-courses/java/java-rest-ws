package ribomation.java_rest_ws.spark_java;

import java.util.Date;

public class Token {
    private String token;
    private String username;
    private Date   created;

    public Token() {
    }

    public Token(String token, String username) {
        this.token = token;
        this.username = username;
        this.created = new Date();
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }
}
