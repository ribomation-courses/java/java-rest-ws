package ribomation.java_rest_ws.ajax.server;

import static spark.Spark.*;

public class AssetsServer {
    public static void main(String[] args) {
        AssetsServer app = new AssetsServer();
        int          port = 8000;
        app.init(port);
        app.routes();
        System.out.printf("Assets server: http://localhost:%d/%n", port);
    }

    private void init(int port) {
        port(port);
        notFound((req, res) -> "Oooops: " + req.url());
        staticFiles.location("/assets");
    }

    private void routes() {
        get("/", (req,res) -> {
            res.redirect("/index.html");
            return null;
        });
    }
}
