package ribomation.java_rest_ws.ajax.server;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import ribomation.java_rest_ws.ajax.Product;
import ribomation.java_rest_ws.ajax.ProductRepo;

import static spark.Spark.*;

public class ApiServer {
    public static void main(String[] args) {
        ApiServer app = new ApiServer();
        int port = 8888;
        app.setup();
        app.init(port);
        app.cors();
        app.routes();
        System.out.printf("API server: http://localhost:%d/%n", port);
    }

    private final String      JSON = "application/json";
    private       ProductRepo repo;
    private       Gson        gson;

    private void setup() {
        repo = new ProductRepo();
        gson = new GsonBuilder()
                .setPrettyPrinting()
                .setDateFormat("yyyy-MM-dd")
                .create();
    }

    private void init(int port) {
        port(port);
        notFound((req, res) -> "Oooops: " + req.url());
    }

    void cors() {
        after((req, res) -> {
            if (!req.requestMethod().equals("OPTIONS")) {
                res.header("Access-Control-Allow-Origin", "*");
            }
        });

        options("/products", JSON, (req, res) -> {
            res.header("Access-Control-Allow-Origin", "*");
            res.header("Access-Control-Allow-Headers", "Content-Type");
            res.header("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE");
            res.status(200);
            return null;
        }, gson::toJson);
    }

    private void routes() {
        path("/products", () -> {
            get("", (req, res) -> repo.getProducts(), gson::toJson);

            get("/:id", (req, res) -> {
                var id = Integer.parseInt(req.params("id"));
                var p  = repo.getProductById(id);
                if (p.isEmpty()) {
                    halt(404);
                }

                return p.get();
            }, gson::toJson);

            delete("/:id", (req, res) -> {
                var id = Integer.parseInt(req.params("id"));
                var p  = repo.getProductById(id);
                if (p.isEmpty()) {
                    halt(404);
                }

                repo.remove(id);
                res.status(204);
                return null;
            }, gson::toJson);

            put("/:id", JSON, (req, res) -> {
                var id = Integer.parseInt(req.params("id"));
                var p  = repo.getProductById(id);
                if (p.isEmpty()) {
                    halt(404);
                }

                var P = gson.fromJson(req.body(), Product.class);
                if (P == null) {
                    halt(400);
                }

                return p.get().update(P);
            }, gson::toJson);

            post("", JSON, (req, res) -> {
                var p = gson.fromJson(req.body(), Product.class);
                if (p == null) {
                    halt(400);
                }
                res.status(201);
                return repo.insert(p);
            }, gson::toJson);
        });
    }
}
