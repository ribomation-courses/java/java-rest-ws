const baseUrl = 'http://localhost:8888';

function populate(product) {
    $('#product-id').text(product.id);
    $('#product-name').text(product.name);
    $('#product-price').text(product.price);
    $('#product-items').text(product.itemsInStock);
    $('#product-modified').text(product.modificationDate);

    $('#product-json').html(JSON.stringify(product, null, 2).replace('\n', '\r\n'));
}

function getProduct(id) {
    console.info('HTTP GET: id:', id);

    $.get(`${baseUrl}/products/${id}`)
        .then((payload) => {
            const product = JSON.parse(payload);
            console.info('product', product);
            populate(product);
            $('#product').show();
        })
        .fail((err) => {
            console.info('FAILURE', err);
            $('#product').hide();
            $('#product-json').text(`FAILURE: ${err.statusText}. ID=${id}`);
        });
}

function createProduct(formId) {
    const data = $(formId)
        .serializeArray()
        .reduce((data, e) => {
            data[e.name] = e.value;
            return data;
        }, {});
    data.modificationDate = new Date().toLocaleDateString();
    return data;
}


$(() => {
    $('#product').hide();

    $('#fetch').click(() => {
        $('#product').hide();
        $('#product-json').text('LOADING...');
        getProduct($('#id').val());
    });

    $('#id').change(() => {
        getProduct($('#id').val());
    });

    $('#create').click(() => {
        $('#product').hide();
        $('#product-json').text('SUBMITTING...');

        const data = createProduct('#newProduct');
        console.info('REQ data:', data);
        $.ajax({
            method: 'POST',
            url: `${baseUrl}/products`,
            contentType: 'application/json',
            data: JSON.stringify(data),
            dataType: 'json',
            crossDomain: true
        })
            .then((product) => {
                console.info('RES data:', product);
                populate(product);
                $('#id').val(product.id);
                $('#product').show();
            })
            .fail((err) => {
                console.error('FAILURE', err);
                $('#product-json').text(`FAILURE: ${err.statusText}`);
            });
    });
});
