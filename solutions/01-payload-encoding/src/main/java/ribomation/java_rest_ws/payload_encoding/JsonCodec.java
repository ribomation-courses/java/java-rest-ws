package ribomation.java_rest_ws.payload_encoding;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

public class JsonCodec implements Codec {
    private final Type TYPE = new TypeToken<ArrayList<Product>>() {}.getType();
    private final Gson gson;

    public JsonCodec() {
        gson = new GsonBuilder()
                .setPrettyPrinting()
                .setDateFormat("yyyy-MM-dd")
                .create();
    }

    @Override
    public String encode(List<Product> products) {
        return gson.toJson(products, TYPE);
    }

    @Override
    public List<Product> decode(String payload) {
        return gson.fromJson(payload, TYPE);
    }

}
