package ribomation.java_rest_ws.payload_encoding;

import org.simpleframework.xml.*;
import org.simpleframework.xml.core.Persister;

import java.io.StringWriter;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

public class XmlCodec implements Codec {

    @Root(name = "product")
    public static class XmlProduct extends Product {
        private Product delegate;

        public XmlProduct(Product delegate) {
            this.delegate = delegate;
        }

        @Attribute
        @Override
        public int getId() {
            return delegate.getId();
        }

        @Attribute
        @Override
        public void setId(int id) {
            delegate.setId(id);
        }

        @Element
        @Override
        public String getName() {
            return delegate.getName();
        }

        @Element
        @Override
        public void setName(String name) {
            delegate.setName(name);
        }

        @Element
        @Override
        public float getPrice() {
            return delegate.getPrice();
        }

        @Element
        @Override
        public void setPrice(float price) {
            delegate.setPrice(price);
        }

        @Element
        @Override
        public int getItemsInStock() {
            return delegate.getItemsInStock();
        }

        @Element
        @Override
        public void setItemsInStock(int itemsInStock) {
            delegate.setItemsInStock(itemsInStock);
        }

        @Element
        @Override
        public Date getModificationDate() {
            return delegate.getModificationDate();
        }

        @Element
        @Override
        public void setModificationDate(Date modificationDate) {
            delegate.setModificationDate(modificationDate);
        }
    }

    @Root(name = "product-list")
    public static class ProductList {
        @ElementList
        List<XmlProduct> products;

        ProductList(List<Product> products) {
            this.products = products.stream()
                    .map(XmlProduct::new)
                    .collect(Collectors.toList());
        }
    }


    @Override
    public String encode(List<Product> products) {
        var p = new ProductList(products);
        try {
            StringWriter buf        = new StringWriter();
            Serializer   serializer = new Persister();
            serializer.write(p, buf);
            return buf.toString();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public List<Product> decode(String payload) {
        return null;
    }
}
