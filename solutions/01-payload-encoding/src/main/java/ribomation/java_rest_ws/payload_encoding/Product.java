package ribomation.java_rest_ws.payload_encoding;

import java.util.Date;
import java.util.Objects;

public class Product {
    private int    id;
    private String name;
    private float  price;
    private int    itemsInStock;
    private Date   modificationDate;

    public Product() {
    }

    public Product(int id, String name, float price, int itemsInStock, Date modificationDate) {
        this.id = id;
        this.name = name;
        this.price = price;
        this.itemsInStock = itemsInStock;
        this.modificationDate = modificationDate;
    }

    @Override
    public String toString() {
        return "Product{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", price=" + price +
                ", itemsInStock=" + itemsInStock +
                ", modificationDate=" + modificationDate +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Product product = (Product) o;
        return id == product.id &&
                Float.compare(product.price, price) == 0 &&
                itemsInStock == product.itemsInStock &&
                name.equals(product.name) &&
                modificationDate.equals(product.modificationDate);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, price, itemsInStock, modificationDate);
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }

    public int getItemsInStock() {
        return itemsInStock;
    }

    public void setItemsInStock(int itemsInStock) {
        this.itemsInStock = itemsInStock;
    }

    public Date getModificationDate() {
        return modificationDate;
    }

    public void setModificationDate(Date modificationDate) {
        this.modificationDate = modificationDate;
    }
}
