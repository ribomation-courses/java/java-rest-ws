package ribomation.java_rest_ws.payload_encoding;

import java.util.List;

public interface Codec {
    String encode(List<Product> products);
    List<Product> decode(String payload);
}
