package ribomation.java_rest_ws.payload_encoding;

import java.util.List;

public class App {
    public static void main(String[] args) {
        App app = new App();
        app.setup();
        app.run();
    }

    private ProductRepo repo;
    private Codec[]     codecs;

    private void setup() {
        repo = new ProductRepo();
        repo.getProducts().forEach(System.out::println);

        codecs = new Codec[]{
                new JsonCodec(),
                new XmlCodec()
        };
    }

    private void run() {
        for (Codec codec : codecs) {
            convert(codec, repo.getProducts());
        }
    }

    private void convert(Codec codec, List<Product> products) {
        String payload = codec.encode(products);
        System.out.printf("%n%n--- %s ---%n%s%n",
                codec.getClass().getSimpleName(), payload);
    }

}
