Java for REST Web Services, 3 days
====

Welcome to this course.
The syllabus can be found at
[java/java-rest-ws](https://www.ribomation.se/courses/java/java-rest-ws.html)

Here you will find
* Installation instructions
* Solutions to the programming exercises
* Source code to demo projects


Installation Instructions
====

Java
----
In order to do the programming exercises of the course, you need to have
Java JDK, the latest version installed.
* [Java JDK Download](http://www.oracle.com/technetwork/java/javase/downloads/)

You also need a decent Java IDE.
* [JetBrains IntellJ IDEA](https://www.jetbrains.com/idea/download)


SDKMAN
----
We are going to use some JVM tools during the course. If you already have
GIT and its GIT-BASH window; you can then install SDKMAN by the following command
inside a BASH window:

    curl -s "https://get.sdkman.io" | bash

Using the `sdk` command you can then install a java tool, such as gradle by:

    sdk install gradle

More info about SDKMAN
* [SDKMAN web](https://sdkman.io/)


Java Libraries
----
During the course we will use several Java libraries, such as Spark-Java,
DavidWebb, Resty, jJWT, that all can be downloaded as a dependency by a gradle build file.


HTTPie
----
HTTPie is a really good command-line tool for interacting with REST
web services. It's implemented in Python, so you need to install Python first.
* [HTTPie - Command line HTTP client](https://httpie.org/)
* [Python 3 - Required by HTTPie](https://www.python.org/downloads/release/python-363/)


GIT Repo
====

You need to have a GIT client installed to clone this repo. Otherwise, you can just click on the download button and grab it all as a ZIP or TAR bundle.

* [GIT Client Download](https://git-scm.com/downloads)

Get the sources initially by a `git clone` operation

    git clone https://gitlab.com/ribomation-courses/java/java-rest-ws.git
    cd java-rest-ws

Get the latest updates by a git pull operation

    git pull

***
*If you have any questions, don't hesitate to contact me*<br>
**Jens Riboe**<br/>
Ribomation AB<br/>
[jens.riboe@ribomation.se](mailto:jens.riboe@ribomation.se)<br/>
[www.ribomation.se](https://www.ribomation.se)<br/>
